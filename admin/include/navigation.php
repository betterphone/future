
<!-- BEGIN CONTAINER -->
<div class="container-fluid" style="padding: 0px;">
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
                    <li class="start <?php echo ($Page == 'home') ? 'active' : '' ?>">
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">
                            <i class="icon-home"></i>
                            <span class="title">
                                Dashboard
                            </span>
                        </a>
                    </li>
                    <?php
                    $_Menus = array(
                        array(
                            'name' => 'Manage Content',
                            'link' => 'javascript:;',
                            'icon' => 'icon-user',
                            'has_submenus' => true,
                            'pages' => array('content'),
                            'submenus' => array(
                                'user' => array(
                                    'name' => 'Work Page',
                                    'link' => make_admin_url('content', 'list', 'list')
                                ),
                            ),
                        ),
                        array(
                            'name' => 'Preferences',
                            'link' => 'javascript:;',
                            'icon' => 'icon-cogs',
                            'has_submenus' => TRUE,
                            'pages' => array('setting'),
                            'submenus' => array(
                                'setting' => array(
                                    'name' => 'Preferences',
                                    'link' => make_admin_url('setting', 'list', 'list', 'sname=general')
                                )
                            )
                        ),
                    );
                    make_admin_menus($_Menus, $Page, $action);
                    ?>

                </ul>
            </div>
        </div>
        <div class="page-content-wrapper">
            <div class="page-content">
