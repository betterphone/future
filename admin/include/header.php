<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title><?php echo SITE_NAME.' | Admin';?></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
        
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
<!--       <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>-->
        <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
        <link href="assets/plugins/pace/themes/pace-theme-barber-shop.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>

        <link href="assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
<!--        <link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>-->
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/plugins.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/custom_default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>

	
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css"/>
    
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2-metronic.css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-tree/bootstrap-tree/css/bootstrap-tree.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jstree/dist/themes/default/style.min.css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css"/>
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toastr/toastr.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/css/iphone-style-checkbox.css"/>
        <link rel="stylesheet" type="text/css" href="assets/css/jquery.raty.css"/>
	<!-- END PAGE LEVEL STYLES -->
        
        
        <!-- BEGIN PAGE LEVEL STYLES for datatables-->
	<link rel="stylesheet" href="assets/plugins/data-tables/DT_bootstrap.css"/>
	<!-- END PAGE LEVEL STYLES -->
        
        <!-- BEGIN PAGE LEVEL STYLES for validation -->
	
        <link rel="stylesheet" type="text/css" href="assets/plugins/validation/validationEngine.jquery.css" />
      
	<!-- END PAGE LEVEL STYLES -->
        
        <!--fancybox-->
         <link href="assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
		<!--fancybox ends-->
        
        
         <!--inbox-->
         <link href="assets/css/pages/inbox.css" rel="stylesheet" type="text/css" />
         <!--inbox ends-->

        <!-- BEGIN CORE PLUGINS -->
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <script src="assets/scripts/status.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
        <script src="assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        
        <script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>  
        <script type="text/javascript" src="assets/plugins/ckfinder/ckfinder.js"></script>
        <script type="text/javascript" src="assets/plugins/ckeditor/adapters/jquery.js"></script> 
        
        <script>
		jQuery(document).ready(function() {       
		
                   
                    var config = {
                        toolbar:
                        [
                                ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
                                ['UIColor', 'Format', 'TextColor', 'Source','PasteText']
                        ],
                        autoParagraph:false
                   };
                   
                   var config_small = {
                        toolbar:
                        [
                                ['Bold', 'Italic', '-', 'Link', 'Unlink'],
                                ['UIColor', 'Format', 'TextColor', 'Source','PasteText']
                        ],
                        height:'80px',
                        autoParagraph:false
                   };

                    var config2 = {
                        toolbar:
                        [
                                ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', '-', 'Source'],
                                ['UIColor', 'Image', 'TextColor', 'BGColor','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ],
                                ['Styles','Format','Font','FontSize'],
                                ['-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl'],
                                ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ]
                        ],
                        autoParagraph:false
                    };

                $('.editor').ckeditor(config);
                $('.editor_small').ckeditor(config_small);
                $('.editor_full').ckeditor(config2);
                CKFinder.setupCKEditor( null, 'assets/plugins/ckfinder/' );
                // Initialize the editor.
                // Callback function can be passed and executed after full instance creation.
                   
                
                   
		});
	</script>
        <!-- END JAVASCRIPTS -->

	<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="<?php echo (BACKEND_STICKY_NAV_BAR=='1')?'page-sidebar-closed':'';?> page-boxed ">
<!-- BEGIN HEADER -->
<div class="header navbar <?php echo (BACKEND_STICKY_NAV_BAR) ? 'navbar-fixed-top' : ''; ?> ">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="header-inner container-fluid">

        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler hidden-phone">
        </div>
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <a class="navbar-brand" href="<?php echo make_admin_url('home', 'list', 'list'); ?>" style="color:#fff;padding-left: 18px;">
            <?php echo SITE_NAME ?>
        </a>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <img src="assets/img/menu-toggler.png" alt=""/>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <div class="hor-menu hidden-sm hidden-xs">
            <ul class="nav navbar-nav">
 
            </ul>
        </div>
        <!-- BEGIN TOP NAVIGATION MENU -->
        <ul class="nav navbar-nav pull-right">
            <li class="dropdown user">
                <a style="line-height:26px;" class="dropdown-toggle" href="<?php echo make_admin_url('setting', 'list', 'list'); ?>">
                    <i class="icon-cogs"></i>
                    <span class="username">Settings</span>
                </a>
            </li>
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user">
                <a style="line-height:26px;" href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-user"></i>
                    <span class="username">Account</span>
                    <i class="icon-angle-down"></i>
                </a>
                <ul class="dropdown-menu">
                    
                    <li>
                        <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=password'); ?>">
                            <i class="icon-lock"></i> Change Password
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo make_admin_url('logout'); ?>">
                            <i class="icon-key"></i> Log Out
                        </a>
                    </li>
                </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>