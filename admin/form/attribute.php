<!-- section coding -->
<?php

/* sections */
switch ($section):
    case 'list':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/list.php');
            break;
    case 'list_set':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/list_set.php');
            break;
    case 'list_super':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/list_super.php');
            break;
    case 'update':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/edit.php');
            break; 
    case 'update_set':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/edit_set.php');
            break;   
    case 'update_super':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/edit_super.php');
            break;   
    case 'insert':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/create.php');
            break;  
    case 'insert_set':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/create_set.php');
            break; 
    case 'insert_super':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/create_super.php');
            break;   
     case 'thrash':
            Redirect(make_admin_url('attribute', 'list', 'list'));
            break;         
    default:break;
endswitch;
?>




