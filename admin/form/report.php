<!-- section coding -->
<?php

/* sections */
switch ($section):
    case 'top_products':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/top_products.php');
	    break; 
    case 'tax':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/tax.php');
	    break; 
    case 'reviews':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/reviews.php');
	    break; 
    case 'customer':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/customer.php');
	    break;
    case 'address':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/address.php');
	    break;
    case 'customer_order':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/customer_order.php');
	    break;
    case 'most_viewed':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/most_viewed.php');
	    break;
    case 'search':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/search.php');
	    break;
    default:break;
endswitch;
?>




