<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title"><i class="icon-group"></i> Manage currency</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a>  
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-group"></i>
                <a href="<?php echo make_admin_url('currency', 'list', 'list'); ?>">List currency</a> 
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                New currency
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <form class="form-horizontal"  method="POST" id="validation">
        <!-- / Box -->
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Add New currency</div>
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">      
                    <div class="form-group">
                        <label class="span2 control-label" for="currencyname">Currency1<span class="required">*</span></label>
                        <div class="span8">
                            <input type="text" name="currency1"  value="USD" id="currencyname" class="span6 form-control validate[required]" readonly />
                        </div>
                    </div>	
                    <div class="form-group">
                        <label class="span2 control-label" for="firstname">Currency2<span class="required">*</span></label>
                        <div class="span8">
                            <input type="text" name="currency2"  value="" id="firstname" class="span6 form-control m-wrap validate[required]" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="span2 control-label" for="firstname">Amount<span class="required">*</span></label>
                        <div class="span8">
                            <input type="text" name="amount"  value="" id="firstname" class="span6 form-control m-wrap validate[required]" />
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="offset2">
                            <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('currency', 'list', 'list'); ?>" class="btn" name="cancel"> Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
