<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-group"></i> Manage Customers</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>   
                        <li>
                                <i class="icon-group"></i>
                                <a href="<?php echo make_admin_url('user', 'update', 'update','id='.$id);?>">Edit Customer</a> 
                                <i class="icon-angle-right"></i>
                        </li>   
                        <li class="last">
                            List Customer Addresses
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    List Customer Addresses - <?php echo $values->firstname.' '.$values->lastname;?>
                </div>
                <div class="actions">
                   <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('user', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >	
                      <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                 <tr>
                                    <th style="width:1%;" class="hidden-480"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th class="hidden-480">Name</th>
                                    <th class="hidden-480">Phone</th>
                                    <th class="hidden-480">Zip Code/City</th>
                                    <th>State/Country</th>
                                    <th>Default</th>
                                    <!--<th>Action</th>-->
                                </tr>
                            </thead>
                            <?php if($QueryObj->GetNumRows()!=0):?>
                                <tbody>
                                <?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):?>
                                    <tr class="odd gradeX">
                                            <td class="hidden-480">
                                                <input class="checkboxes" id="multiopt[<?php echo $object->id ?>]" name="multiopt[<?php echo $object->id ?>]" type="checkbox" />
                                            </td>
                                            <td><?php echo $sr?>.</td>
                                            <td class="hidden-480"><?php echo $object->address_name;?></td>
                                            <td class="hidden-480"><?php echo $object->firstname." ".$object->lastname;?></td>
                                            <td class="hidden-480"><?php echo $object->phone;?></td>
                                            <td class="hidden-480"><?php echo $object->zip_code." / ".$object->city;?></td>
                                            <td><?php echo state_name($object->state_id)." / ".country_name($object->country_id);?></td>
                                            <td><?php echo ($object->is_default)?"Yes":"No";?></td>
                                            <!--<td>
                                                <a href="<?php echo make_admin_url('user', 'update_address', 'update_address', 'id='.$object->id)?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-share"></i> View</a>
                                                <a href="<?php echo make_admin_url('user', 'delete_address', 'address', 'aid='.$object->id.'&id='.$id)?>" onclick="return confirm('Are you sure? You are deleting this record.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i> Delete</a>
                                            </td>-->
                                    </tr>
                                <?php $sr++; endwhile;?>
                                </tbody>
                                <!--<tfoot>
                                    <tr class="odd gradeX hidden-480">
                                        <td colspan="3">
                                            <div style=" width:220px;float:left">
                                                <select name="multiopt_action" style="width:150px;float: left;" class="left_align form-control">
                                                    <option value="delete">Delete</option>
                                                </select>
                                                <input style="float:right" type="submit" class="btn blue large" name="multiopt_go" value="Go"  onclick="return confirm('Are you sure?');" />
                                            </div>
                                        </td>
                                        <td></td><td></td>
                                        <td></td><td></td>
                                        <td></td>
                                        <td></td>
                                    </tr> 
                                </tfoot>-->
                           <?php endif;?>  
                        </table>
                </form>    
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>