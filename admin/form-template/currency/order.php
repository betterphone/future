<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-group"></i> Manage Customers</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>   
                        <li>
                                <i class="icon-group"></i>
                                <a href="<?php echo make_admin_url('user', 'update', 'update','id='.$id);?>">Edit Customer</a> 
                                <i class="icon-angle-right"></i>
                        </li>   
                        <li class="last">
                            List Customer Orders
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    List Customer Orders - <?php echo $values->firstname.' '.$values->lastname;?>
                </div>
                <div class="actions">
                   <?php include_once(DIR_FS_SITE_ADMIN.'/form-template/'.$modName.'/shortcut.php');?>  
                </div>
            </div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('user', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >	
                      <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                 <tr>
                                    <th style="width:1%;" class="hidden-480"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th>
                                    <th>#</th>
                                    <th>Order #</th>
                                    <th>Purchased On</th>
                                    <th class="hidden-480">Product Quantity</th>
                                    <th class="hidden-480">Payment Method</th>
                                    <th class="hidden-480">Order Price(<?php echo CURRENCY_SYMBOL;?>)</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <?php if(!empty($orders)):?>
                                <tbody>
                                <?php $sr=1;foreach($orders as $kk=>$vv):?>
                                    <tr class="odd gradeX">
                                            <td class="hidden-480">
                                                <input class="checkboxes" id="multiopt[<?php echo $vv['id']; ?>]" name="multiopt[<?php echo $vv['id']; ?>]" type="checkbox" />
                                            </td>
                                            <td><?php echo $sr?>.</td>
                                            <td class="hidden-480"><?php echo $vv['id'];?></td>
                                            <td class="hidden-480"><?php echo $vv['date_add'];?></td>
                                            <td class="hidden-480"><?php echo $vv['totalquantity'];?></td>
                                            <td class="hidden-480"><?php echo $vv['payment_method_name'];?></td>
                                            <td><?php echo show_price($vv['grand_total']);?></td>
                                            <td><?php echo $vv['current_state'];?></td>
                                            <td>
                                                <a href="<?php echo make_admin_url('order', 'update', 'update', 'id='.$vv['id'].'&redirect=user')?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-share"></i> View</a>
                                            </td>
                                    </tr>
                                <?php $sr++; endforeach;?>
                                </tbody>
                           <?php endif;?>  
                        </table>
                </form>    
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>