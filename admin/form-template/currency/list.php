<div class="row-fluid">
    <div class="span12">
        <h3 class="page-title"><i class="icon-group"></i> Manage Users</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                   
            <li class="last">
                List Users
            </li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<?php
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Users</div>
                <div class="actions"><?php include_once 'shortcut.php'; ?>
                </div>
            </div>
            <div class="portlet-body">
                <form method="POST" id="form_data" name="form_data" >	
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>currency1</th>
                                <th>currency2</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($lists as $key => $list) { ?>
                                <tr>
                                    <td><?php echo $key+=1 ?></td>
                                    <td><?php echo $list['currency1'] ?></td>
                                    <td><?php echo $list['currency2'] ?></td>
                                    <td><?php echo $list['amount'] ?></td>
                                    <td>
                                        <a id="do_action" href="<?php echo make_admin_url($Page, 'update', 'update', 'id=' . $list['id']) ?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i> &nbsp;</a>
                                        <a href="<?php echo make_admin_url($Page, 'delete', 'list', 'id=' . $list['id']) ?>" onclick="return confirm('Are you sure? You are deleting this record.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i> &nbsp;</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </form>    
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>