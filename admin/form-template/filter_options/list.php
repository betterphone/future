<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-krw"></i> Manage Filter Options</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                            Filter Options
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    Filter Options
                </div>
                <div class="actions">
                  
                </div>
            </div>
            <div class="note note-info">
                    <p>
                        You can set position of filter options here.<br/>
                        Filter options can be added from product option panel.
                    </p>
            </div>
            <?php if(count($options)) :?>
            <div class="well">
                <form method="post" id="validate1" action="<?php echo make_admin_url('order','update','update','id='.$id);?>" name="change_current_state">
                     <label class="span2">Select Option</label>
                    <div class="span3">
                    <select class="form-control" name="type" id="option_value_selected">
                        <?php foreach ($options as $kk=>$vv): ?>
                                <option value="<?php echo $vv->id;?>" <?php echo ($option_id==$vv->id)?"selected":"";?>><?php echo ucwords($vv->name);?></option>
                        <?php endforeach;?>
                        <input type="hidden" disabled id="url_page_base_url" value="<?php echo make_admin_url('filter_options','list','list','option_id=');?>"/>
                    </select>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix margin-bottom-20"></div>
            <?php endif;?>
            
            <div class="portlet-body">
               <table class="table table-striped table-bordered table-hover" >
                    <thead>
                         <tr>
                            <th width="75%">Option</th>
                            <th width="25%">Action</th>
                        </tr>
                    </thead>
                        <?php if(!empty($option_values)):?>
                            <tbody>
                            <?php $sr=1;foreach($option_values as $kk=>$vv):?>
                                <tr class="odd gradeX">
                                        <td><?php echo $vv->option_value;?></td>
                                        <td>
                                            <input module="product_option" type="number" name="position[<?php echo $vv->id?>]" row_id="<?php echo $vv->option_value ?>"  value="<?php echo $vv->position;?>" style="width:50%;" class="position_update"/>
                                        </td>
                                </tr>
                            <?php $sr++;endforeach;?>
                            </tbody>
                       <?php endif;?>  
                    </table>  
            </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>