<?php if($action!='list'):?>
<a href="<?php echo make_admin_url('brand','list','list');?>" class="btn default blue-stripe">
    <i class="icon-angle-left"></i>
    <span class="hidden-480">
             Back
    </span>
</a>
<?php endif;?>
<?php if($action!='insert' && $action!='thrash'):?>
<a href="<?php echo make_admin_url('brand','insert','insert');?>" class="btn default blue-stripe">
    <i class="icon-plus"></i>
    <span class="hidden-480">
             Add Brand
    </span>
</a>
<?php endif;?>
<?php if($action=='update'):?>
<a href="<?php echo make_admin_url('brand_product','list','list','id='.$id);?>" class="btn default blue-stripe">
    <i class="icon-barcode"></i>
    <span class="hidden-480">
             View Brand Products
    </span>
</a>    
<?php endif; ?>
<?php if($action!='thrash'):?>
<a href="<?php echo make_admin_url('brand','thrash','thrash');?>" class="btn default blue-stripe">
    <i class="icon-trash"></i>
    <span class="hidden-480">
             Trash
    </span>
</a>
<?php endif;?>
