
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">

            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Level Settings > <?php echo ucwords(str_replace('_', ' ', $sname)); ?>
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>Level Settings</li>

            </ul>


            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- / Box -->
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-cogs"></i>
                        <?php echo ucwords(str_replace('_', ' ', $sname)); ?> Level </div>
                    <div class="tools">
                    </div>
                </div>
                <div class="portlet-body form form-body"> 


                        <form class="form-horizontal" action="<?php echo make_admin_url('level', 'update', 'list'); ?>" method="POST" enctype="multipart/form-data" id="validation">
                            <?php foreach ($ob as $kk => $vv): ?>
                                <?php if($vv['key']=='SITEMAP_PATH' || $vv['key']=='LAST_SITEMAP_DATE' ): ?>
                                    <div class="form-group">
                                        <label class="control-label col-md-4"><?php echo get_setting_name($vv['title']); ?>:</label>
                                        <div class="controls col-md-8" style="padding-top: 8px;">
                                            <?php if($vv['value']!=''): ?>
                                                <code><?php echo $vv['value']; ?></code>
                                            <?php endif;?>
                                            <span class="clearfix"></span>
                                            <span class="help-block settings_help"><?php echo $vv['hint']; ?></span>
                                        </div>
                                    </div>
                                <?php else :?>
                                    <div class="form-group <?php echo $vv['key'];?>">
                                        <label class="control-label col-md-4"><?php echo get_setting_name($vv['title']); ?>:</label>
                                        <div class="controls col-md-8">
                                            <?php echo get_setting_control($vv['id'], $vv['type'], $vv['value'], $vv['options']); ?>
                                            <span class="clearfix"></span>
                                            <span class="help-block settings_help">
                                                <?php echo $vv['hint']; ?>
                                                <?php if($vv['key']=='TERMS_CONDITIONS_CHECKBOX'):?>
                                                <br/>Edit Terms and conditions page content 
                                                <a target="_blank" href="<?php echo make_admin_url('content','update','update','id='.TERMS_CONDITIONS_PAGE) ?>">here</a>.
                                                <?php endif;?>
                                            </span>
                                        </div>
                                    </div>
                                <?php endif;?>
                            <?php endforeach; ?>   
                            <div class="form-actions fluid">
                                <div class="offset2">
                                    <input  type="hidden" name="sname1" value="<?php echo $sname ?>"/>
                                    <input  type="hidden" name="setting_type" value="<?php echo $setting_type ?>"/>
                                    <button class="btn green" type="submit" name="Submit" value="Submit"><i class="icon-edit"></i> Save</button> 
                                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>" class="btn" name="cancel"> Cancel</a>
                                </div>
                            </div>
                        </form>
                   
                </div>
            </div>                             
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    
<script type="text/javascript">
    function ChangeToPassField() {
        if(document.getElementById("show_password").checked==true){
            document.getElementById('password').type="text";
        }
        if(document.getElementById("show_password").checked==false){
            document.getElementById('password').type="password";
        }
    }
    
    $(document).ready(function(){
        var field_location_class = window.location.hash.replace('#','');
        if(field_location_class!=''){
            $('.'+field_location_class).attr('id','highlight');
            $('html, body').animate({
                scrollTop: $("."+field_location_class).offset().top-100
            }, 1000);
        }
    });
</script>