<div class="row-fluid">
    <div class="span12">
        <h3 class="page-title"><i class="icon-group"></i> Manage fee</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                   
            <li class="last">
                List fee
            </li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<?php
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List fee</div>
                <div class="actions"><?php include_once 'shortcut.php'; ?>
                </div>
            </div>
            <div class="portlet-body">
                <form method="POST" id="form_data" name="form_data" >	
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Fee</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($lists as $key => $list) { ?>
                                <tr>
                                    <td><?php echo $key+=1 ?></td>
                                    <td><?php echo $list['from_'] ?></td>
                                    <td><?php echo $list['to_'] ?></td>
                                    <td><?php echo $list['fee'] ?></td>
                                    <td>
                                        <a id="do_action" href="<?php echo make_admin_url($Page, 'update', 'update', 'id=' . $list['id']) ?>"  title="click here to_ edit this record" class="btn btn-xs default"><i class="icon-pencil"></i> &nbsp;</a>
                                        <a href="<?php echo make_admin_url($Page, 'delete', 'list', 'id=' . $list['id']) ?>" onclick="return confirm('Are you sure? You are deleting this record.');"  title="click here to_ delete this record" class="btn btn-xs default"><i class="icon-trash"></i> &nbsp;</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </form>    
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>