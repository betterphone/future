<div class="main_home">
    <div class="row-fluid">
        <h3 class="section_title">CONTENTS</h3>
        <a href="<?php echo make_admin_url('content', 'list', 'list'); ?>">
            <div class="tile bg-dark">
                <div class="tile-body">
                    <i class="icon-group"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Contents
                    </div>
                    <div class="number">
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="clearfix"></div>
    
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>