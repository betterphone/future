<div class="row-fluid">
    <div class="span12">
        <h3 class="page-title"><i class="icon-list-alt"></i> Manage Content Pages</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                   
            <li class="last">
                List Content Pages
            </li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<?php
display_message(1);
?>

<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">List Pages</div>
                <div class="actions">
                    <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>
            </div>
            <div class="portlet-body">
                <form method="POST">
                    <div class="row-fluid">
                        <div class="span4">
                            <select class="form-control" name="type">
                                <option value="" <?php echo $type == '' ? 'selected' : ''?>>All</option>
                                <option value="home" <?php echo $type == 'home' ? 'selected' : ''?>>Home</option>
                                <option value="about" <?php echo $type == 'about' ? 'selected' : ''?>>About</option>
                                <option value="service" <?php echo $type == 'service' ? 'selected' : ''?>>Service</option>
                                <option value="work" <?php echo $type == 'work' ? 'selected' : ''?>>Work</option>
                            </select>
                        </div>
                        <div class="span2">
                            <button type="submit" name="filter_submit" class="btn blue">Submit</button>
                        </div>
                    </div>
                </form>
                <br />
                <form action="<?php echo make_admin_url('content', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >	
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th style="width:10%;" class="hidden-480">Sr. No.</th>
                                <th>Page Name</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($listContent as $key => $values) { ?>
                                <tr class="odd gradeX">
                                    <td style="text-align:center;" class="hidden-480"><?php echo $key+=1; ?></td>
                                    <td  class="clickable"><?php echo $values['name']; ?></td>
                                    <td  class="clickable"><?php echo $values['type']; ?></td>
                                    <td>
                                        <a id="do_action" href="<?php echo make_admin_url('content', 'update', 'update', 'id=' . $values['id']) ?>"  title="click here to edit this record" class="btn btn-xs default"><i class="icon-pencil"></i></a>
                                        <a href="<?php echo make_admin_url('content', 'delete', 'list', 'id=' . $values['id']) ?>" onclick="return confirm('Are you sure? You are deleting this page.');"  title="click here to delete this record" class="btn btn-xs default"><i class="icon-trash"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </form>    
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>