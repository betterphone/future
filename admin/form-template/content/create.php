<div class="row-fluid">
    <div class="span12">
        <h3 class="page-title"><i class="icon-list-alt"></i> Manage Content Pages</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                <i class="icon-angle-right"></i>
            </li>                                  
            <li>
                <i class="icon-list-alt"></i>
                <a href="<?php echo make_admin_url('content', 'list', 'list'); ?>">List Pages</a>
                <i class="icon-angle-right"></i>                                       
            </li>
            <li class="last">
                New Page
            </li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<?php
display_message(1);
?>
<div class="clearfix"></div>
<div class="row-fluid">
    <form class="form-horizontal" action="<?php echo make_admin_url('content', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">
        <div class="span12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">Edit Page</div>
                    <div class="actions">
                        <?php include_once(DIR_FS_SITE_ADMIN . '/form-template/' . $modName . '/shortcut.php'); ?>  
                    </div>
                </div>
                <div class="portlet-body form form-body">      
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Name<span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="text" name="name"  value="" id="name" class="form-control m-wrap validate[required]" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="description">Page Content</label>
                        <div class="col-md-8">
                            <textarea id="page" class="form-control m-wrap" name="page" rows="6"></textarea>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="description">Which Content</label>
                        <div class="col-md-8">
                            <select id="type" class="form-control m-wrap validate[required]" name="type">
                                <option>Choose type</option>
                                <option value="home">Home</option>
                                <option value="about">About</option>
                                <option value="service">Service</option>
                                <option value="work">Work</option>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="description">Image</label>
                        <div class="col-md-8">
                            <input type="file" name="image" />
                        </div>
                    </div> 


                    <div class="form-actions fluid">
                        <div class="offset2">
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('content', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>