<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-search"></i> Search Terms</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                             Search Terms
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"> Search Terms (<?php echo $from_date." to ".$to_date;?>)</div>
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn default blue-stripe" href="<?php echo make_admin_url('setting','list','list').'#REMOVE_WORDS_FROM_SEARCH';?>">
                            <i class="icon-strikethrough"></i>
                            <span class="hidden-480">
                                 Filter Search Words
                            </span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a class="btn default blue-stripe" href="<?php echo make_admin_url('report','search','search','to_date='.$to_date.'&from_date='.$from_date.'&print=1');?>">
                            <i class="icon-print"></i>
                            <span class="hidden-480">
                                 Print Report
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="well">
                <form id="validate1" action="<?php echo make_admin_url('report','search','search');?>" name="search_report">
                    <input type="hidden" name="Page" value="report"/>
                    <input type="hidden" name="action" value="search"/>
                    <input type="hidden" name="section" value="search"/>
                    <div class="span5">
                        <div class="input-group input-large">
                            <input type="text" readonly class="form-control sql_format_datepicker" name="from_date" value="<?php echo $from_date;?>"/>
                            <span class="input-group-addon">to</span>
                            <input type="text" readonly class="form-control sql_format_datepicker" name="to_date" value="<?php echo $to_date;?>"/>
                        </div>
                    </div>
                    <div class="span5">
                    <button type="submit" class="btn btn-sm green"><i class="icon-check"></i> Submit</button>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="portlet-body">
                <form action="<?php echo make_admin_url('report', 'search', 'search');?>" method="post" id="form_data" name="form_data" >	
                 <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                 <tr>
                                    <th style="width:1%;" class="hidden-480">
                                        <!--<input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />-->
                                    </th>
                                    <!--<th>#</th>-->
                                    <th>Keyword</th>
                                    <th>Occurrence</th>
                                </tr>
                            </thead>
                            <?php if(!empty($records)):?>
                                <tbody>
                                <?php $sr=1;foreach($records as $kk=>$vv):?>
                                    <tr class="odd gradeX">
                                            <td class="hidden-480">
                                                <input class="checkboxes" id="multiopt[<?php echo $sr;?>]" name="multiopt[<?php echo $vv->keyword ?>]" type="checkbox" />
                                            </td>
                                            <!--<td><?php echo $sr?>.</td>-->
                                            <td><?php echo $vv->keyword;?></td>
                                            <td><?php echo $vv->total_hits;?></td>
                                    </tr>
                                <?php $sr++; endforeach;?>
                                </tbody>
                                <tfoot>
                                    <tr class="odd gradeX hidden-480">
                                        <td colspan="3">
                                            <div style=" width:220px;float:left">
                                                <select name="multiopt_action" style="width:150px;float: left;" class="left_align form-control">
                                                    <option value="synonyms">Create Synonyms</option>
                                                    <option value="hypernoms">Create Hypernoms</option>
                                                </select>
                                                <input style="float:right" type="submit" class="btn blue large" name="multiopt_go" value="Go" />
                                            </div>
                                        </td>
                                    </tr> 
                                </tfoot>
                           <?php endif;?>  
                        </table>
                </form>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>