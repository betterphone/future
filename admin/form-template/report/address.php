<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-group"></i> Address Report </h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                             Customers Addresses
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"> Address Report (<?php echo $from_date." to ".$to_date;?>)</div> 
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn default blue-stripe" href="<?php echo make_admin_url('report','address','address','to_date='.$to_date.'&from_date='.$from_date.'&print=1');?>">
                            <i class="icon-print"></i>
                            <span class="hidden-480">
                                 Print Report
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="well">
                <form id="validate1" action="<?php echo make_admin_url('report','address','address');?>" name="address_report">
                    <input type="hidden" name="Page" value="report"/>
                    <input type="hidden" name="action" value="address"/>
                    <input type="hidden" name="section" value="address"/>
                    <div class="span5">
                        <div class="input-group input-large">
                            <input type="text" readonly class="form-control sql_format_datepicker" name="from_date" value="<?php echo $from_date;?>"/>
                            <span class="input-group-addon">to</span>
                            <input type="text" readonly class="form-control sql_format_datepicker" name="to_date" value="<?php echo $to_date;?>"/>
                        </div>
                    </div>
                    <div class="span5">
                    <button type="submit" class="btn btn-sm green"><i class="icon-check"></i> Submit</button>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="portlet-body">
                 <table class="table table-striped table-bordered table-hover" id="sample_21">
                            <thead>
                                 <tr>
                                    <th>ID</th>
                                    <th>Username</th>
                                    <th class="hidden-480">Address Title</th>
                                    <th>Name</th>
                                    <th class="hidden-480">City / State</th>
                                    <th>Country / Zip Code</th>
                                    <th class="hidden-480">Phone</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($records)):?>
                                <?php $sr=1;foreach($records as $kk=>$vv):?>
                                    <tr class="odd gradeX">
                                            <td><?php echo $vv->id?></td>
                                            <td><?php echo $vv->username;?></td>
                                            <td class="hidden-480"><?php echo $vv->address_name;?></td>
                                            <td><?php echo $vv->firstname." ".$vv->lastname;?></td>
                                            <td class="hidden-480"><?php echo $vv->city." / ".$vv->state;?></td>
                                            <td><?php echo $vv->country." / ".$vv->zip_code;?></td>
                                            <td class="hidden-480"><?php echo $vv->phone;?></td>
                                    </tr>
                                <?php $sr++; endforeach;?>
                            <?php else: ?>
                                <tr class="odd gradeX hidden-480">
                                    <td colspan='8'>No new address in the given time frame.</td>
                                </tr>
                           <?php endif;?>  
                           </tbody>
                        </table>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>