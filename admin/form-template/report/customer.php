<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-group"></i> Customer Report </h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                             New Customers 
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"> Customer Report (<?php echo $from_date." to ".$to_date;?>)</div> 
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn default blue-stripe" href="<?php echo make_admin_url('report','customer','customer','to_date='.$to_date.'&from_date='.$from_date.'&print=1');?>">
                            <i class="icon-print"></i>
                            <span class="hidden-480">
                                 Print Report
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="well">
                <form id="validate1" action="<?php echo make_admin_url('report','customer','customer');?>" name="change_current_state">
                    <input type="hidden" name="Page" value="report"/>
                    <input type="hidden" name="action" value="customer"/>
                    <input type="hidden" name="section" value="customer"/>
                    <div class="span5">
                        <div class="input-group input-large">
                            <input type="text" readonly class="form-control sql_format_datepicker" name="from_date" value="<?php echo $from_date;?>"/>
                            <span class="input-group-addon">to</span>
                            <input type="text" readonly class="form-control sql_format_datepicker" name="to_date" value="<?php echo $to_date;?>"/>
                        </div>
                    </div>
                    <div class="span5">
                    <button type="submit" class="btn btn-sm green"><i class="icon-check"></i> Submit</button>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="portlet-body">
                 <table class="table table-striped table-bordered table-hover" id="sample_21">
                            <thead>
                                 <tr>
                                    <th>Time Period</th>
                                    <th>Number of Users</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr class="odd gradeX">
                                            <td><?php echo $from_date." to ".$to_date;?></td>
                                            <td><?php echo $records;?></td>
                                    </tr>
                           </tbody>
                        </table>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>