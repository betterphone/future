<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-comments-alt"></i> Product Reviews </h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                             Reviews
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"> Product Reviews Report</div>
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn default blue-stripe" href="<?php echo make_admin_url('report','reviews','reviews','print=1');?>">
                            <i class="icon-print"></i>
                            <span class="hidden-480">
                                 Print Report
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="portlet-body">
                 <table class="table table-striped table-bordered table-hover" id="sample_21">
                            <thead>
                                 <tr>
                                    <th>#</th>
                                    <th class="hidden-480">ID</th>
                                    <th>Product Name</th>
                                    <th class="hidden-480">SKU</th>
                                    <th class="hidden-480">UPC</th>
                                    <th>Avg. Rating</th>
                                    <th>Total Reviews</th>
                                    <th>Last Review</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($records)):?>
                                <?php $sr=1;foreach($records as $kk=>$vv):?>
                                    <tr class="odd gradeX">
                                            <td><?php echo $sr?>.</td>
                                            <td class="hidden-480"><?php echo $vv->product_id;?></td>
                                            <td><?php echo $vv->product_name;?></td>
                                            <td class="hidden-480"><?php echo $vv->sku;?></td>
                                            <td class="hidden-480"><?php echo $vv->upc;?></td>
                                            <td><?php echo $vv->rating;?></td>
                                            <td><?php echo $vv->reviews;?></td>
                                            <td><?php echo $vv->last_review;?></td>
                                    </tr>
                                <?php $sr++; endforeach;?>
                            <?php else: ?>
                                <tr class="odd gradeX">
                                    <td colspan='8'>Still there are no reviews.</td>
                                </tr>
                           <?php endif;?>  
                           </tbody>
                        </table>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>