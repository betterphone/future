<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
        <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title"><i class="icon-pushpin"></i> Top Products Report</h3>
                <ul class="page-breadcrumb breadcrumb">
                        <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                <i class="icon-angle-right"></i>
                        </li>                                   
                        <li class="last">
                             Top Products Report
                        </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
</div>
<!-- END PAGE HEADER-->
<div class="clearfix"></div>
<?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>

<div class="clearfix"></div>
  <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"> Top Products Report (<?php echo $from_date." to ".$to_date;?>)</div>
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn default blue-stripe" href="<?php echo make_admin_url('report','top_products','top_products','to_date='.$to_date.'&from_date='.$from_date.'&print=1');?>">
                            <i class="icon-print"></i>
                            <span class="hidden-480">
                                 Print Report
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="well">
                <form id="validate1" action="<?php echo make_admin_url('report','top_products','top_products');?>" name="change_current_state">
                    <input type="hidden" name="Page" value="report"/>
                    <input type="hidden" name="action" value="top_products"/>
                    <input type="hidden" name="section" value="top_products"/>
                    <div class="span5">
                        <div class="input-group input-large">
                            <input type="text" readonly class="form-control sql_format_datepicker" name="from_date" value="<?php echo $from_date;?>"/>
                            <span class="input-group-addon">to</span>
                            <input type="text" readonly class="form-control sql_format_datepicker" name="to_date" value="<?php echo $to_date;?>"/>
                        </div>
                    </div>
                    <div class="span5">
                    <button type="submit" class="btn btn-sm green"><i class="icon-check"></i> Submit</button>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="portlet-body">
                 <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                 <tr>
                                    <th>#</th>
                                    <th class="hidden-480">ID</th>
                                    <th>Name</th>
                                    <th>Variant</th>
                                    <th class="hidden-480">SKU #</th>
                                    <th>Quantity Sold</th>
                                </tr>
                            </thead>
                            <?php if(!empty($records)):?>
                                <tbody>
                                <?php $sr=1;foreach($records as $kk=>$vv):?>
                                    <tr class="odd gradeX">
                                            <td><?php echo $sr?>.</td>
                                            <td class="hidden-480"><?php echo $vv['product_id'];?></td>
                                            <td>
                                                <a href="<?php echo make_admin_url('product','update','update','id='.$vv['product_id']);?>">
                                                    <?php echo $vv['product_name'];?>
                                                </a>
                                            </td>
                                            <td><?php echo ($vv['variant_name']!='')?$vv['variant_name']:"--";?></td>
                                            <td class="hidden-480"><?php echo $vv['product_sku'];?></td>
                                            <td><?php echo $vv['total_sale'];?></td>
                                    </tr>
                                <?php $sr++; endforeach;?>
                                </tbody>
                           <?php endif;?>  
                        </table>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
 <div class="clearfix"></div>