<?php

include_once(DIR_FS_SITE . 'include/functionClass/contentClass.php');

$modName = 'content';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;
isset($_POST['type']) ? $type = $_POST['type'] : $type = '';

switch ($action):
    case 'list':
        $query = new content();
        $listContent = $query->listContent($type);

        break;

    case 'update':
        $query = new content();
        $list_content = $query->list_content($id);
        if (isset($_POST['submit'])) {
            $query = new content();
            $query->saveContent($_POST);
            $admin_user->set_pass_msg('Content Updated successfully');
            Redirect(make_admin_url('content', 'list', 'list'));
        }
        break;

    case 'update2':
        break;

    case 'update_multiple_thrash':
        break;

    case 'insert':
        if (isset($_POST['submit'])) {
            $query = new content();
            $query->print =1;
            $query->saveContent($_POST);
            Redirect(make_admin_url('content', 'list', 'list'));
        }
        break;

    case 'delete':
        $query = new content();
        $query->deleteContent($id);
        $admin_user->set_pass_msg('Content Deleted successfully');
        Redirect(make_admin_url('content', 'list', 'list'));
        break;

    case'permanent_delete':
        break;

    case'restore':
        break;

    case'thrash':
        break;
    default:break;
endswitch;
?>