<?php

$modName='hypernym';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id=0;
isset($_GET['keys'])?$keys=$_GET['keys']:$keys='';
isset($_GET['redirect'])?$redirect=$_GET['redirect']:$redirect='synonym';

/*Handle actions here.*/
switch ($action):
	case 'list':
                $QueryObj= new dictionary_hypernym();
                $QueryObj->listHypernym();
		break;
	case 'update':
                /* update hypernym */
		if(isset($_POST['submit'])):
                        /*server side validation*/
                        $validation=new user_validation();
                        $validation->add('name', 'req');
                        $validation->add('name', 'reg_words');
                        
                        $validation->add('keywords', 'req');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                            $QueryObj = new dictionary_hypernym();  
                            $new_id = $QueryObj ->saveHypernym($_POST);
                            if($new_id):
                                $admin_user->set_pass_msg('Hypernym has been updated successfully.');
                            else:
                                $admin_user->set_pass_msg('An error occurred while updating new hypernym.');
                            endif;
                            Redirect(make_admin_url('hypernym', 'update', 'update', 'id='.$new_id));
                        endif;
		endif;
		
                /* get hypernym hypernyms */
                $Query_obj= new dictionary_hypernym();
                $values=$Query_obj->getHypernym($id);
                
                if(!is_object($values)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something went wrong.');
                    Redirect(make_admin_url('hypernym', 'list', 'list'));
                endif;
		break;

	case 'insert':
                /* update hypernym */
		if(isset($_POST['submit'])):
                        /*server side validation*/
                        $validation=new user_validation();
                        $validation->add('name', 'req');
                        $validation->add('name', 'reg_words');
                        
                        $validation->add('keywords', 'req');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        if($error!='1'): /*if there is no error*/
                            $QueryObj = new dictionary_hypernym();  
                            $new_id = $QueryObj ->saveHypernym($_POST);
                            if($new_id):
                                $admin_user->set_pass_msg('Hypernym has been added successfully.');
                                if($redirect=='report'):
                                    Redirect(make_admin_url('report', 'search', 'search'));
                                else:
                                    Redirect(make_admin_url('hypernym', 'update', 'update', 'id='.$new_id));
                                endif;
                            else:
                                $admin_user->set_pass_msg('An error occurred while adding new hypernym.');
                                Redirect(make_admin_url('hypernym', 'insert', 'insert'));
                            endif;
                        endif;
                        
		endif;
                break;
            
        case 'delete':
                $hypernym= new dictionary_hypernym(); 
                $hypernym->deletehypernym($id);
                
                $admin_user->set_pass_msg('Hypernym has been deleted successfully.');
                Redirect(make_admin_url('hypernym', 'list', 'list'));
                break;
            
        case 'update2':
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                    if($_POST['multiopt_action']=='delete'):
                        if(count($_POST['multiopt'])):
                            foreach($_POST['multiopt'] as $k=>$v):
                                   $deleteObj= new dictionary_hypernym(); 
                                   $deleteObj->deletehypernym($k);
                            endforeach;
                         else:
                            $admin_user->set_error();   
                            $admin_user->set_pass_msg('Please select atleast one item for operation');
                            Redirect(make_admin_url('hypernym', 'list', 'list'));
                         endif;   
                    endif;
                endif;
            
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('hypernym', 'list', 'list'));
                break;   
    default:break;
endswitch;
?>