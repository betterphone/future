<?php
include_once(DIR_FS_SITE.'include/functionClass/productClass.php');
include_once(DIR_FS_SITE.'include/functionClass/attributeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/taxClass.php');
include_once(DIR_FS_SITE.'include/functionClass/searchClass.php');

$modName='report';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['print'])?$print=$_GET['print']:$print='0';
isset($_GET['user_id'])?$user_id=$_GET['user_id']:$user_id='0';
isset($_GET['to_date'])?$to_date=$_GET['to_date']:$to_date=date('Y-m-d');
isset($_GET['from_date'])?$from_date=$_GET['from_date']:$from_date=date('Y-m-d',strtotime("-1 month", time()));

switch ($action):
	case 'top_products':
            $Query = new order_detail();
            $records = $Query -> getTopOrderedProducts($from_date,$to_date);
            
            if($print):
              download_csv_file($records,'top_products');  
              Redirect(make_admin_url('report','top_products','top_products'));
            endif;
            
            break;
        
        case 'tax':
            $Query = new order_detail();
            $records = $Query -> getTotalOrderTax($from_date,$to_date);

            $calculated_tax = array();
            $total_tax = '0';

            if(!empty($records)):
                $sr = '1';
                foreach($records as $kk=>$vv):
                    $total_tax = $total_tax + $vv['total_tax'];
                    if($sr=='1'):
                        $tax_rule_group = $vv['tax_rule_group_id'];
                        $tax_rule_group_name= $vv['tax_rule_group_name'];
                        $tax= '0';
                    endif;
                    if($tax_rule_group!=$vv['tax_rule_group_id']):
                        $calculated_tax[] = array(
                            'tax_rule_group_id' => $tax_rule_group,
                            'tax_rule_group_name' => $tax_rule_group_name,
                            'tax' => $tax
                        );
                        $tax_rule_group = $vv['tax_rule_group_id'];
                        $tax_rule_group_name= $vv['tax_rule_group_name'];
                        $tax= '0';
                    endif;  
                    
                    $tax = $tax + $vv['total_tax'];
                    
                    if($sr==count($records)):
                        $calculated_tax[] = array(
                            'tax_rule_group_id' => $tax_rule_group,
                            'tax_rule_group_name' => $tax_rule_group_name,
                            'tax' => $tax
                        );
                        $calculated_tax[] = array(
                            'tax_rule_group_id' => '',
                            'tax_rule_group_name' => 'Total Tax',
                            'tax' => $total_tax
                        );
                        $tax_rule_group = $vv['tax_rule_group_id'];
                        $tax= '0';
                    endif;
                    $sr++;
                endforeach;
            endif;
            if($print):
              download_csv_file($calculated_tax,'tax');  
              Redirect(make_admin_url('report','top_products','top_products'));
            endif;

            break;
            
      case 'reviews':
            $Query = new product_review();
            $records = $Query -> listProductReviews();

            if(!empty($records)):
                foreach($records as $kk=>$vv):
                    $Query = new product_review();
                    $rate = $Query->getAggrigateProductRate($vv->product_id);
                    $records[$kk]->rating = $rate;
                endforeach;
            endif;

            if($print):
              download_csv_file($records,'Product_Reviews');  
              Redirect(make_admin_url('report','reviews','reviews'));
            endif;
            break;
            
       case 'customer':
            $Query = new user();
            $records = $Query -> getCountofNewAccounts($from_date,$to_date);
            
            if($print):
              $output = array();
              $output[] = array('Time Frame'=>$from_date.' to '.$to_date,'Number of Users'=>$records); 
              download_csv_file($output,'New_Customers');  
              Redirect(make_admin_url('report','customer','customer'));
            endif;
            break; 
         
      case 'address':
            $Query = new user_address();
            $records = $Query -> ListAddresses($from_date,$to_date);
            
            if($print):
              download_csv_file($records,'New_Addresses');  
              Redirect(make_admin_url('report','address','address'));
            endif;
            break; 
        
      case 'customer_order':
            $QueryObj= new user();
            $users = $QueryObj->listUsers(false,'array');
            
            $user_orders = array();
            if($user_id!='0'):
                /* get user orders */
                $Query_orders = new order();
                $user_orders = $Query_orders->getUserOrders($user_id);
            endif;

            if($print):
              $output=array();
              if(!empty($user_orders)):
                 foreach($user_orders as $kk=>$vv): 
                    $output[$kk]['Order ID'] = $vv['id'];
                    $output[$kk]['Billing User'] = $vv['billing_firstname']." ".$vv['billing_lastname'];
                    $output[$kk]['Quantity'] = $vv['totalquantity'];
                    $output[$kk]['Order Price'] = $vv['grand_total'];
                 endforeach;
              endif;

              download_csv_file($output,'User_Orders');  
              Redirect(make_admin_url('report','customer_order','customer_order'));
            endif;
            break; 
            
      case 'most_viewed':
            $Query = new product_views();
            $records = $Query -> getMostViewedProducts($from_date,$to_date);
            
            if($print):
              download_csv_file($records,'Most_Viewed_Products');  
              Redirect(make_admin_url('report','most_viewed','most_viewed'));
            endif;
            
            break;   
            
      case 'search':
            $Query = new search();
            $records = $Query -> getSearchTerms($from_date,$to_date);
            
            if($print):
              download_csv_file($records,'Search_Terms');  
              Redirect(make_admin_url('report','search','search'));
            endif;
            
            if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                if(count($_POST['multiopt'])):
                    if($_POST['multiopt_action']=='synonyms'):
                        $synonyms = '';
                        foreach($_POST['multiopt'] as $k=>$v):
                               $synonyms .=trim($k).',';
                        endforeach;
                        Redirect(make_admin_url('synonym', 'insert', 'insert','keys='.$synonyms.'&redirect=report'));
                    endif;
                    if($_POST['multiopt_action']=='hypernoms'):
                        $hypernoms = '';
                        foreach($_POST['multiopt'] as $k=>$v):
                               $hypernoms .=trim($k).',';
                        endforeach;
                        Redirect(make_admin_url('hypernym', 'insert', 'insert','keys='.$hypernoms.'&redirect=report'));
                    endif;
                 else:
                    $admin_user->set_error();   
                    $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                    Redirect(make_admin_url('report', 'search', 'search'));
                 endif;   
            endif;
            
            break;      
    default:break;
endswitch;
?>