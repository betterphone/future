<?php

$modName='synonym';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id=0;
isset($_GET['keys'])?$keys=$_GET['keys']:$keys='';
isset($_GET['redirect'])?$redirect=$_GET['redirect']:$redirect='synonym';

/*Handle actions here.*/
switch ($action):
	case 'list':
                $QueryObj= new dictionary_synonym();
                $QueryObj->listSynonym();
		break;
	case 'update':
                /* update synonym */
		if(isset($_POST['submit'])):
                        /*server side validation*/
                        $validation=new user_validation();
                        $validation->add('keywords', 'req');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        
                        if($error!='1'): /*if there is no error*/
                            $QueryObj = new dictionary_synonym();  
                            $new_id = $QueryObj ->saveSynonym($_POST);
                            if($new_id):
                                $admin_user->set_pass_msg('Synonym has been updated successfully.');
                            else:
                                $admin_user->set_pass_msg('An error occurred while updating new synonym.');
                            endif;
                            Redirect(make_admin_url('synonym', 'update', 'update', 'id='.$new_id));
                        endif;
		endif;
		
                /* get synonym synonyms */
                $Query_obj= new dictionary_synonym();
                $values=$Query_obj->getSynonym($id);
                
                if(!is_object($values)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something went wrong.');
                    Redirect(make_admin_url('synonym', 'list', 'list'));
                endif;
		break;

	case 'insert':
                /* update synonym */
		if(isset($_POST['submit'])):
                        /*server side validation*/
                        $validation=new user_validation();
                        $validation->add('keywords', 'req');
                        
                        $valid= new valid();
                        
                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/
                            $error_obj->errorAddArray($valid->error);
                        endif;
                        if($error!='1'): /*if there is no error*/
                            $QueryObj = new dictionary_synonym();  
                            $new_id = $QueryObj ->saveSynonym($_POST);
                            if($new_id):
                                
                                $admin_user->set_pass_msg('Synonym has been added successfully.');
                                if($redirect=='report'):
                                    Redirect(make_admin_url('report', 'search', 'search'));
                                else:
                                    Redirect(make_admin_url('synonym', 'update', 'update', 'id='.$new_id));
                                endif;
                                
                            else:
                                $admin_user->set_pass_msg('An error occurred while adding new synonym.');
                                Redirect(make_admin_url('synonym', 'insert', 'insert'));
                            endif;
                        endif;
                        
		endif;
                break;
            
        case 'delete':
                $synonym= new dictionary_synonym(); 
                $synonym->deleteSynonym($id);
                
                $admin_user->set_pass_msg('Synonym has been deleted successfully.');
                Redirect(make_admin_url('synonym', 'list', 'list'));
                break;
            
        case 'update2':
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                    if($_POST['multiopt_action']=='delete'):
                        if(count($_POST['multiopt'])):
                            foreach($_POST['multiopt'] as $k=>$v):
                                   $deleteObj= new dictionary_synonym(); 
                                   $deleteObj->deleteSynonym($k);
                            endforeach;
                         else:
                            $admin_user->set_error();   
                            $admin_user->set_pass_msg('Please select atleast one item for operation');
                            Redirect(make_admin_url('synonym', 'list', 'list'));
                         endif;   
                    endif;
                endif;
            
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('synonym', 'list', 'list'));
                break;   
    default:break;
endswitch;
?>