<?php

/** DoDirectPayment NVP example; 
 *
 *  Process a credit card payment. 
 *
 * Send HTTP POST Request
 *
 * @param	string	The API method name
 * @param	string	The POST Message fields in &name=value pair format
 * @return	array	Parsed HTTP Response body
 */
function PPHttpPost($methodName_, $nvpStr_,$payment_method) {
        
        if(IS_FEED):
            if(SUPPLIER_PAYPAL_SANDBOX):
                $environment = 'sandbox';
            else:
                $environment = 'live';
            endif;
            $API_UserName = urlencode(SUPPLIER_PAYPAL_API_USERNAME); // set your spi username
            $API_Password = urlencode(SUPPLIER_PAYPAL_API_PASSSWORD); // set your spi password
            $API_Signature = urlencode(SUPPLIER_PAYPAL_API_SIGNATURE); // set your spi Signature
            
        else:
            if($payment_method->is_sandbox):
                $environment = 'sandbox';
            else:
                $environment = 'live';
            endif;
            $API_UserName = urlencode($payment_method->api_username); // set your spi username
            $API_Password = urlencode($payment_method->api_password); // set your spi password
            $API_Signature = urlencode($payment_method->api_signature); // set your spi Signature
        endif;
	
	
	$API_Endpoint = "https://api-3t.paypal.com/nvp";
	if("sandbox" === $environment || "beta-sandbox" === $environment) {
            $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
	}
        
	$version = urlencode('51.0');
 
	// Set the curl parameters.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
 
	// Turn off the server and peer verification (TrustManager Concept).
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
 
	// Set the API operation, version, and API signature in the request.
	$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

	// Set the request as a POST FIELD for curl.
	curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
 
	// Get response from the server.
	$httpResponse = curl_exec($ch);
 
        /*
	if(!$httpResponse) {
		exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
	}
        */
	// Extract the response details.
	$httpResponseAr = explode("&", $httpResponse);
 
	$httpParsedResponseAr = array();
	foreach ($httpResponseAr as $i => $value) {
		$tmpAr = explode("=", $value);
		if(sizeof($tmpAr) > 1) {
			$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
		}
	}
        /*
	if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
		exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
	}
        */
	return $httpParsedResponseAr;
}

$form=array();
$form['PAYMENTACTION']='sale';
$form['AMT']=number_format($order->grand_total, 2);
$form['CREDITCARDTYPE']=urlencode($_POST['customer_credit_card_type']);
$form['ACCT']=urlencode($_POST['customer_credit_card_number']);
$form['EXPDATE']=urlencode(str_pad($_POST['cc_expiration_month'], 2, '0', STR_PAD_LEFT)).urlencode($_POST['cc_expiration_year']);
$form['CVV2']=urlencode($_POST['cc_cvv2_number']);
$form['FIRSTNAME']=urlencode($_POST['customer_first_name']);
$form['LASTNAME']=urlencode($_POST['customer_last_name']);
$form['STREET']= urlencode($_POST['customer_address1']);
$form['CITY']= urlencode($_POST['customer_city']);
$form['STATE']= urlencode($_POST['customer_state']);
$form['ZIP']= urlencode($_POST['customer_zip']);
$form['COUNTRYCODE']= urlencode($_POST['customer_country']);
$form['CURRENCYCODE']= urlencode('USD');

/*


$items = '';
$quantity = 1;
foreach($order->items as $kk=>$vv):
    $items.=$vv['product_name'].'-'.$vv['product_variant_value'];
    $items.=', ';
    $quantity = $vv['quantity']+$quantity;
endforeach;
$form['items']=substr($items,0,strlen($items)-2); 	#items detail
$form['grand_total']=number_format($order->grand_total, 2);
//$form['quantity']=$quantity;
$form['business']=$business;
$form['cmd']='_xclick';
$form['currency_code']='USD';
$form['return']=DIR_WS_SITE.'include/pg/paypal/success.php';
$form['notify_url']=DIR_WS_SITE.'include/pg/paypal/ipn.php';
$form['cancel_return']=DIR_WS_SITE.'include/pg/paypal/fail.php';
$form['item_name']=$items;
$form['amount']=number_format($order->total_price_tax_excl, 2);
$form['orderid']=$order->id;
$form['custom']=$order->id;
$form['no_notes']='1';
$form['shipping']=number_format($order->total_shipping_amount, 2);
//$form['discount_amount']=number_format($order->total_discount, 2);
$form['tax']=number_format($order->total_tax, 2);
$form['name']=$order->billing_firstname.' '.$order->billing_lastname;
$form['address1']=$order->billing_address1;
$form['address2']=$order->billing_address2;
$form['city']=$order->billing_city;
$form['state']=$order->billing_state;
$form['country']=$order->billing_country;
$form['email']=$order->billing_email;
$form['zip']=$order->billing_zip;

 
// Set request-specific fields.
$paymentType = urlencode('Sale');				// 'Authorization' or 'Sale'
$firstName = urlencode($_POST['customer_first_name']);
$lastName = urlencode($_POST['customer_last_name']);
$creditCardType = urlencode($_POST['customer_credit_card_type']);
$creditCardNumber = urlencode($_POST['customer_credit_card_number']);
$expDateMonth = $_POST['cc_expiration_month'];
// Month must be padded with leading zero
$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
 
$expDateYear = urlencode($_POST['cc_expiration_year']);
$cvv2Number = urlencode($_POST['cc_cvv2_number']);
$address1 = urlencode($_POST['customer_address1']);
$address2 = urlencode($_POST['customer_address2']);
$city = urlencode($_POST['customer_city']);
$state = urlencode($_POST['customer_state']);
$zip = urlencode($_POST['customer_zip']);
$country = urlencode($_POST['customer_country']);				// US or other valid country code
$amount = urlencode($_POST['example_payment_amuont']);
$currencyID = urlencode('USD');							// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
 
// Add request-specific fields to the request string.
$nvpStr =	"&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
			"&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
			"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";

// Execute the API operation; see the PPHttpPost function above.
$httpParsedResponseAr = PPHttpPost('DoDirectPayment', $nvpStr);
 
if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
	exit('Direct Payment Completed Successfully: '.print_r($httpParsedResponseAr, true));
} else  {
	exit('DoDirectPayment failed: ' . print_r($httpParsedResponseAr, true));
}
   * 
 */
?>