<?php

if(defined("SUPPLIER_PAYPAL_ACCOUNT_EMAIL") && IS_FEED):
   $business=SUPPLIER_PAYPAL_ACCOUNT_EMAIL;
   if(SUPPLIER_PAYPAL_SANDBOX):
       $payment_url='https://www.sandbox.paypal.com/cgi-bin/webscr/'; 
   else:
       $payment_url='https://www.paypal.com/cgi-bin/webscr/'; 
   endif;
else:
   $business=$payment_method->paypal_business; 
   if($payment_method->is_sandbox):
       $payment_url='https://www.sandbox.paypal.com/cgi-bin/webscr/'; 
   else:
       $payment_url='https://www.paypal.com/cgi-bin/webscr/'; 
   endif;
endif;

$items = '';
$quantity = 1;
foreach($order->items as $kk=>$vv):
    $items.=$vv['product_name'].'-'.$vv['product_variant_value'];
    $items.=', ';
    $quantity = $vv['quantity']+$quantity;
endforeach;

$form=array();
$form['items']=substr($items,0,strlen($items)-2); 	#items detail
$form['grand_total']=number_format($order->grand_total, 2);
//$form['quantity']=$quantity;
$form['business']=$business;
$form['cmd']='_xclick';
$form['currency_code']='USD';
$form['return']=DIR_WS_SITE.'include/pg/paypal/success.php';
$form['notify_url']=DIR_WS_SITE.'include/pg/paypal/ipn.php';
$form['cancel_return']=DIR_WS_SITE.'include/pg/paypal/fail.php';
$form['item_name']=$items;
$form['amount']=number_format($order->grand_total, 2);
$form['orderid']=$order->id;
$form['custom']=$order->id;
$form['no_notes']='1';
//$form['shipping']=number_format($order->total_shipping_amount, 2);
//$form['discount_amount']=number_format($order->total_discount, 2);
//$form['tax']=number_format($order->total_tax, 2);
$form['name']=$order->billing_firstname.' '.$order->billing_lastname;
$form['address1']=$order->billing_address1;
$form['address2']=$order->billing_address2;
$form['city']=$order->billing_city;
$form['state']=$order->billing_state;
$form['country']=$order->billing_country;
$form['email']=$order->billing_email;
$form['zip']=$order->billing_zip;
?>