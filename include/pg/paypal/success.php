<?php

include('../../config/config.php');
include_once(DIR_FS_SITE.'include/functionClass/productClass.php');
include_once(DIR_FS_SITE.'include/functionClass/searchClass.php');

#include basic functions
$include_fucntions=array('email');
include_functions($include_fucntions);

/** Update our database with the results from the Notification POST **/	
if(isset($_REQUEST['custom']) && isset($_REQUEST['txn_id']) && $_REQUEST['txn_id']!=''):

    // TODO: perform appropriate action on success or add curl checkup if ipn request not performed 
    // ipn file not work on local so action performed on success instead on ipn on localhost 
    // @todo for testing purpose
    if($_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='ecomp2' || $_SERVER['HTTP_HOST']=='ecom'):
        $q= new order;
        $q->Data['id']=$_REQUEST['custom'];
        $q->Data['total_paid'] = isset($_REQUEST['mc_gross'])?$_REQUEST['mc_gross']:""; 
        $q->Data['transaction_id'] = $_REQUEST['txn_id']; 
        $q->Data['is_payment_made'] = 1; 
        $q->Data['payment_time']=date('Y-m-d H:i:s');
        $q->Data['current_state']='Payment Received';
        $q->Update();

        order::OrderSuccess($_REQUEST['custom']);
    endif;
    //$login_session->pass_msg[]=show("Order Placed successfully.",false);
    //$login_session->set_pass_msg();
    $login_session->add_popup('ORDER_PLACED_SUCCESS','Order Placed successfully.','success');
    $_SESSION['user_session']['order_id'] = $_REQUEST['custom'];
    Redirect(make_url('order','id='.$_REQUEST['custom'].'&success=1'));
else:
    redirect(make_url('404'));
endif;
?>