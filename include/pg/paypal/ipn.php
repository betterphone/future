<?php

include('../../config/config.php');
include_once(DIR_FS_SITE.'include/functionClass/productClass.php');
include_once(DIR_FS_SITE.'include/functionClass/searchClass.php');

#include basic functions
$include_fucntions=array('email');
include_functions($include_fucntions);

/** Update our database with the results from the Notification POST **/	
if(isset($_REQUEST['custom']) && isset($_REQUEST['txn_id']) && $_REQUEST['txn_id']!=''):
    $q= new order;
    $q->Data['id']=$_REQUEST['custom'];
    $q->Data['total_paid'] = isset($_REQUEST['mc_gross'])?$_REQUEST['mc_gross']:""; 
    $q->Data['transaction_id'] = $_REQUEST['txn_id']; 
    $q->Data['is_payment_made'] = 1; 
    $q->Data['payment_time']=date('Y-m-d H:i:s');
    $q->Data['current_state']='Payment Received';
    $q->Update();

    order::OrderSuccess($_REQUEST['custom']);
else:
    redirect(make_url('404'));
endif;
?>