<?php

if ($_SERVER['HTTP_HOST'] == 'localhost') {
    //Local path
    define("HTTP_SERVER", "http://" . $_SERVER['HTTP_HOST'] . "", true);
    define("HTTP_SERVER_SSL", "https://" . $_SERVER['HTTP_HOST'] . "", true);

    //Set website 
    define("DIR_WS_SITE", HTTP_SERVER . "/future/", true);
    define("DIR_WS_SITE_SSL", HTTP_SERVER_SSL . "/future/", true);

    // Database Settings 
    $DBDataBase = "freaksol_future";
    $DBUserName = "root";
    $DBPassword = "";
    $DBHostName = "localhost";
}
if ($_SERVER['HTTP_HOST'] == 'www.freaksolution.com' || $_SERVER['HTTP_HOST'] == 'freaksolution.com') {
    // Server path 
    define("HTTP_SERVER", "http://freaksolution.com/future", true);
    define("HTTP_SERVER_SSL", "http://freaksolution.com/", true);

    // Set website 
    define("DIR_WS_SITE", HTTP_SERVER . "/", true);
    define("DIR_WS_SITE_SSL", HTTP_SERVER_SSL . "/future", true);

    // Database Settings 
    $DBDataBase = "freaksol_future";
    $DBUserName = "freaksol";
    $DBPassword = "mypass007";
    $DBHostName = "localhost";
}
?>