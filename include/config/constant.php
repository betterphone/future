<?php

/* WEBSITE CONSTANTS */
$constants = new query('setting');
$constants->DisplayAll();
while ($constant = $constants->GetObjectFromRecord()):
    define("$constant->key", html_entity_decode($constant->value), true);
endwhile;

// @todo we add these constants in settings for user control 
define("SOFT_DELETE", true);
define("LOGO_PATH", DIR_WS_SITE_GRAPHIC . "logo.png", true);
define("MAXIMUM_GROUPED_PRODUCTS_LISTING", 7);
define("FRAUD_DETECTION_TIME_LIMIT", 24);
define('MOBILE_NAVIGATION_ID', 3, true);
define('SHOW_GOOGLE_TRACKING_CODE_ON_ORDER', 1, true);
define('GOOGLE_CONVERSION_ID', 966767965, true);




/* put these constants in admin panel under settings */
//define('MAIN_NAVIGATION_ID', 1, true);
//define('BOTTOM_NAVIGATION_ID', 2, true);
//define('SITE_TAG_LINE','Made with <small><span class="glyphicon glyphicon-heart tertiary-color"></span></small>', true);
//define('BOTTOM_LEFT_TEXT','© Copyright 2014.');
//define('BOTTOM_RIGHT_TEXT','Made with <span class="glyphicon glyphicon-heart"></span>');
//define("SITE_CACHE_STATUS",FALSE);
//define("SITE_DEBUG_STATUS",FALSE);
//define("SEND_NEW_RESIGTER_EMAIL_TO_ADMIN",1);
//define("SEND_NEW_ORDER_EMAIL_TO_ADMIN",1);
//define("SLIDER_ACTIVE",1,true);//TRUE OR FALSE
//define("SHOW_SLIDER_ON_PAGES","home",true);//HOME OR ALL
//define("SHOW_SLIDER_ON_MOBILE","home",true);//TRUE OR FALSE
//define("SHOW_CURRENCY_ON_TOP",1,true);//TRUE OR FALSE
//define("SHOW_LANGUAGE_ON_TOP",1,true);//TRUE OR FALSE
//define("CURRENCY_SYMBOL", "$");
//define("CURRENCY_SYMBOL_LOCATION", "before");//before or after
//define("WEIGHT_DIMENTIONS", "kg");
//define("DIMENTIONS_UNIT", "cm");
//define("FEATURED_PRODUCTS_HOMEPAGE", 4);//count
//define("LATEST_PRODUCTS_HOMEPAGE", 12);//count
//define("SHOW_OUTOFSTOCK_PRODUCTS", false);//TRUE OR FALSE
//define("ALLOW_BACKORDERS", false);//TRUE OR FALSE
//define("LATEST_PRODUCTS_DAY_LIMIT",90);//days
//define("STOCK_MANAGEMENT",true);// still using after order success only - to reduce product qunatity
//define("CHECKOUT_TYPE",'3');//ONE PAGE CHECKOUT/ THREE STEP CHECKOUT
//define("ENABLE_GUEST_CHECKOUT",TRUE);//TRUE OR FALSE
//define("TERMS_SERVICES_CHECKOUT_CHECKOUT_PAGE",false);//TRUE OR FALSE
//define("SHOW_CART_TOTALS_ON_CART_PAGE",TRUE);//TRUE FALSE
//define("MINIMUM_QUANTITY_REQUIRED_FOR_IN_STOCK",'0');//minimum quantity required for in stock
//define("LOW_STOCK_WARNING",'0');//minimum quantity required for in stock
//define("STOCK_MANAGEMENT",true);// still using after order success only - to reduce product qunatity
//define("IS_SHIPPING_FREE",TRUE);//TRUE FALSE
//define("APPLY_TAX",TRUE);//TRUE FALSE

/* email templates */
define("EMAIL_NEW_RESIGTRATION_USER_CONFIRMATION", '1');
define("EMAIL_NEW_RESIGTRATION_ADMIN", '2');
define("EMAIL_VERIFY_ACCOUNT_USER", '3');
define("EMAIL_CONTACT_US_ADMIN", '4');
define("EMAIL_ORDER_SUCCESS_USER", '5');
define("EMAIL_ORDER_STATUS_ADMIN", '6');
define("EMAIL_FORGOT_PASSWORD", '7');
define("EMAIL_ACCOUNT_CREATED_FROM_ADMIN", '8');
define("EMAIL_LOW_STOCK_NOTIFICATION_ADMIN", '9');
define("EMAIL_PASSWORD_CHANGE_USER", '10');
define("EMAIL_NEWSLETTER_SUBSCRIBE_USER", '11');
define("EMAIL_NEWSLETTER_UNSUBSCRIBE_USER", '12');
define("EMAIL_ORDER_SHIPPING_USER", '13');
define("EMAIL_PASSWORD_UPDATE_ADMIN", '14');
define("EMAIL_ORDER_STATUS_UPDATE_SHIPPED", '15');
define("EMAIL_SHARE_WISHLIST", '25');
define("COMMON_HEADER", '26');
define("COMMON_FOOTER", '27');
define("EMAIL_NEW_RESIGTRATION_USER", '29');
define("EMAIL_RESEND_EMAIL_CONFIRMATION", '30');
define("EMAIL_USERNAME_UPDATE", '31');


/* PHP Validation types */
/*
  define('VALIDATE_REQUIRED', "req", true);
  define('VALIDATE_EMAIL',"email", true);
  define("VALIDATE_MAX_LENGTH","maxlength");
  define("VALIDATE_MIN_LENGTH","minlength");
  define("VALIDATE_NUMERIC","num");
  define("VALIDATE_ALPHA","alpha");
  define("VALIDATE_ALPHANUM","alphanum");
  define("SUBJECT_NEW_ORDER","Order");
  define("SUBJECT_NEW_ORDER_CUSTOMER","Customer Order");
  define("TEMPLATE","default");

  define("ADD_ATTRIBUTE_PRICE_TO_PRODUCT_PRICE", true);
  define("ATTRIBUTE_PRICE_OVERLAP", false);

  define('VERIFY_EMAIL_ON_REGISTER', true);
  define('ERROR_EMAIL', 'rocky.developer004@gmail.com', true);

  $conf_shipping_type=array('quantity', 'subtotal');

  define('SHIPPING_TYPE', 'price');		// price/quantity/weight

  define("STOCK_THRESHOLD", 3);
  define("REVIEW_FOR_BUYER", 0);
  define("REVIEW_FOR_USER", 1);
 */
$AllowedImageTypes = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$AllowedFileTypes = array('application/vnd.ms-excel');

/* new allowed photo mime type array. */
$conf_allowed_file_mime_type = array('text/plain', 'application/msword', 'application/rtf', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'applications/vnd.pdf', 'application/pdf');
$conf_allowed_photo_mime_type = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$conf_order_status = array('Received', 'Processing', 'Shipped', 'Delivered');

define("DOC_TYPE", '<!DOCTYPE html>', true);
define("DOC_LANGUAGE", "en", true);
define("DOC_CHAR_SET", 'utf-8', true);

$not_to_open_page = array();

$imageThumbConfig = array(
    'content' => array(
        'thumb' => array('width' => '50', 'height' => '50'),
        'small' => array('width' => '190', 'height' => '140'),
        'medium' => array('width' => '300', 'height' => '195'),
        'big' => array('width' => '800', 'height' => '600')
    ),
);
/* define shipping types - ids */
define("SHIPPING_FLAT_ID", '1');
define("SHIPPING_PRICE_ID", '2');
define("SHIPPING_WEIGHT_ID", '3');
define("SHIPPING_FEDEX_ID", '4');
define("SHIPPING_LOCAL_PICKUP_ID", '5');

/** defining tables for status on - off in admin as per module name* */
$moduleTableForStatus = array(
    'brand' => 'brand',
    'templates' => 'formula_template',
    'supplier' => 'supplier',
    'category' => 'category',
    'product' => 'product',
    'country' => 'country',
    'state' => 'state',
    'city' => 'city',
    'zone' => 'zone',
    'zone_detail' => 'zone_detail',
    'shipping' => 'shipping',
    'shipping_group' => 'shipping_group',
    'shipping_flat' => 'shipping_flat',
    'shipping_price' => 'shipping_price',
    'shipping_weight' => 'shipping_weight',
    'tax' => 'tax',
    'tax_rule' => 'tax_rule',
    'cart_rule' => 'cart_rule',
    'user' => 'user',
    'manufacturer' => 'manufacturer',
    'banner' => 'banner',
    'blog' => 'blog',
    'blog_comment' => 'blog_comment',
    'payment_method' => 'payment_method',
    'option' => 'option',
    'attribute_super_set' => 'attribute_super_set',
    'attribute' => 'attribute',
    'newsletter' => 'newsletter',
    'product_option' => 'product_option',
    'navigation' => 'navigation',
    'navigations' => 'navigations'
);

/*
 * all order status
 */
$all_order_status = array(
    "Pending Payment" => "Pending Payment",
    "Confirmed" => "Confirmed",
    "Payment Failed" => "Payment Failed",
    "On hold" => "On hold",
    "Canceled" => "Canceled",
    "Suspected Fraud" => "Suspected Fraud",
    "Payment Received" => "Payment Received",
    "Payment Refund" => "Payment Refund",
    "Order Invoiced" => "Order Invoiced",
    "Order Shipped" => "Order Shipped",
    "Completed" => "Completed",
    "Download Permissions Granted" => "Download Permissions Granted"
);

/*
 * pages for banner
 */
$banner_pages_array = array(
    'home' => 'Home Page',
    'shop' => 'Category Pages',
    'mega_menus' => 'Mega Menus',
    'brand' => 'Brand Pages',
    'content' => 'Content Pages',
    'product' => 'Product Pages',
    'cart' => 'Cart Page',
    'checkout' => 'Checkout Page',
    'register' => 'Register Page',
    'blog' => 'Blog Page',
);
/*
 * positions for banner on one page
 * also define height width for each position below
 */
$banner_locations_on_each_page1 = array(
    '1' => 'Top',
    '2' => 'Bottom',
    '3' => 'Left',
    '4' => 'Right',
    '5' => 'Landing Page'
);
/*
 * define height width for each position
 */
$banner_locations_on_each_page = array(
    'Top' => array(
        'width' => '100%',
        'height' => 'auto'
    ),
    'Bottom' => array(
        'width' => '100%',
        'height' => 'auto'
    ),
    'Left' => array(
        'width' => '100%',
        'height' => 'auto'
    ),
    'Right' => array(
        'width' => '100%',
        'height' => 'auto'
    ),
    'Landing_Page' => array(
        'width' => '100%',
        'height' => 'auto'
    ),
);

/*
 * sitemap priorities
 */
$xml_sitemap_priorities = array(
    '1' => 'Highest Priority',
    '0.9' => '',
    '0.8' => '',
    '0.7' => '',
    '0.6' => '',
    '0.5' => 'Medium Priority',
    '0.4' => '',
    '0.3' => '',
    '0.2' => '',
    '0.1' => 'Lowest Priority',
);

/*
 * modules for url rewrite
 */
$modules_for_url_rewrite = array(
    'product' => 'product',
    'category' => 'category',
    'content' => 'content',
    'brand' => 'brand',
    'blog' => 'blog'
);

/*
 * colors array
 */
$all_true_Colors_array = array(
    'ANTIQUE WHITE' => '#FAEBD7',
    'AQUA' => '#00FFFF',
    'AQUAMARINE' => '#7FFFD4',
    'AZURE' => '#F0FFFF',
    'BEIGE' => '#F5F5DC',
    'BISQUE' => '#FFE4C4',
    'BLACK' => '#000000',
    'BLANCHED ALMOND' => '#FFEBCD',
    'BLUE' => '#0000FF',
    'BLUE VIOLET' => '#8A2BE2',
    'BROWN' => '#A52A2A',
    'BURLY WOOD' => '#DEB887',
    'CADET BLUE' => '#5F9EA0',
    'CHARTREUSE' => '#7FFF00',
    'CHOCOLATE' => '#D2691E',
    'CORAL' => '#FF7F50',
    'CORN FLOWER BLUE' => '#6495ED',
    'CORN SILK' => '#FFF8DC',
    'CRIMSON' => '#DC143C',
    'CYAN' => '#00FFFF',
    'DARK BLUE' => '#00008B',
    'DARK CYAN' => '#008B8B',
    'DARK GOLDENROD' => '#B8860B',
    'DARK GRAY' => '#A9A9A9',
    'DARK GREEN' => '#006400',
    'DARK KHAKI' => '#BDB76B',
    'DARK MAGENTA' => '#8B008B',
    'DARK OLIVE GREEN' => '#556B2F',
    'DARK ORANGE' => '#FF8C00',
    'DARK ORCHID' => '#9932CC',
    'DARK RED' => '#8B0000',
    'DARK SALMON' => '#E9967A',
    'DARK SEA GREEN' => '#8FBC8F',
    'DARK SLATE BLUE' => '#483D8B',
    'DARK SLATE GRAY' => '#2F4F4F',
    'DARK TURQUOISE' => '#00CED1',
    'DARK VIOLET' => '#9400D4',
    'DEEP PINK' => '#FF1493',
    'DEEP SKY BLUE' => '#00BFFF',
    'DULL GRAY' => '#696969',
    'DODGER BLUE' => '#1E90FF',
    'FIRE BRICK' => '#B22222',
    'FLORAL WHITE' => '#FFFAF0',
    'FOREST GREEN' => '#228B22',
    'FUCHSIA' => '#FF00FF',
    'GAINSBORO' => '#DCDCDC',
    'GHOST WHITE' => '#F8F8FF',
    'GOLD' => '#FFD700',
    'GOLDENROD' => '#DAA520',
    'GRAY' => '#808080',
    'GREEN' => '#008000',
    'GREEN YELLOW' => '#ADFF2F',
    'HONEYDEW' => '#F0FFF0',
    'HOT PINK' => '#FF69B4',
    'INDIAN RED' => '#CD5C5C',
    'INDIGO' => '#4B0082',
    'IVORY' => '#FFFFF0',
    'KHAKI' => '#F0E68C',
    'LAVENDER' => '#E6E6FA',
    'LAVENDER BLUSH' => '#FFF0F5',
    'LEAF' => '    #6B8E23',
    'LEMON CHIFFON' => '#FFFACD',
    'LIGHT BLUE' => '#ADD8E6',
    'LIGHT CORAL' => '#F08080',
    'LIGHT CYAN' => '#E0FFFF',
    'LIGHT GOLDENROD YELLOW' => '#FAFAD2',
    'LIGHT GREEN' => '#90EE90',
    'LIGHT GRAY' => '#D3D3D3',
    'LIGHT PINK' => '#FF86C1',
    'LIGHT SALMON' => '#FFA07A',
    'LIGHT SEA GREEN' => '#20B2AA',
    'LIGHT SKY BLUE' => '#87CEFA',
    'LIGHT STEEL BLUE' => '#778899',
    'LIGHT YELLOW' => '#FFFFE0',
    'LIME' => '#00FF00',
    'LIME GREEN' => '#32CD32',
    'LINEN' => '#FAF0E6',
    'MAGENTA' => '#FF00FF',
    'MAROON' => '#800000',
    'MEDIUM AQUA MARINE' => '#66CDAA',
    'MEDIUM BLUE' => '#0000CD',
    'MEDIUM ORCHID' => '#BA55D3',
    'MEDIUM PURPLE' => '#9370DB',
    'MEDIUM SEA GREEN' => '#3CB371',
    'MEDIUM SLATE BLUE' => '#7B68EE',
    'MEDIUM SPRING BLUE' => '#00FA9A',
    'MEDIUM TURQUOISE' => '#48D1CC',
    'MEDIUM VIOLET RED' => '#C71585',
    'MIDNIGHT BLUE' => '#191970',
    'MINT CREAM' => '#F5FFFA',
    'MISTY ROSE' => '#FFE4E1',
    'NAVAJO WHITE' => '#FFDEAD',
    'NAVY' => '#000080',
    'OLD LACE' => '#FDF5E6',
    'OLIVE' => '#808000',
    'ORANGE' => '#FFA500',
    'ORANGE RED' => '#FF4500',
    'ORCHID' => '#DA70D6',
    'PALE GOLDENROD' => '#EEE8AA',
    'PALE GREEN' => '#98FB98',
    'PALE TURQUOISE' => '#AFEEEE',
    'PALE VIOLET RED' => '#DB7093',
    'PAPAYAWHIP' => '#FFEFD5',
    'PEACH PUFF' => '#FFDAB9',
    'PERU' => '#CD853F',
    'PINK' => '#FFC0CB',
    'PLUM' => '#DDA0DD',
    'POWDER BLUE' => '#B0E0E6',
    'PURPLE' => '#800080',
    'RED' => '#FF0000',
    'ROSY BROWN' => '#BC8F8F',
    'ROYAL BLUE' => '#4169E1',
    'SADDLE BROWN' => '#8B4513',
    'SALMON' => '#FA8072',
    'SANDY BROWN' => '#F4A460',
    'SEA GREEN' => '#2E8B57',
    'SEASHELL' => '#FFF5EE',
    'SIENNA' => '#A0522D',
    'SILVER' => '#C0C0C0',
    'SKY BLUE' => '#87CEEB',
    'SLATE BLUE' => '#6A5ACD',
    'SLATE GRAY' => '#708090',
    'SNOW' => '#FFFAFA',
    'SPRING GREEN' => '#00FF7F',
    'STEEL BLUE' => '#4682B4',
    'TAN' => '#D2B48C',
    'TEAL' => '#008080',
    'THISTLE' => '#D88FD8',
    'TOMATO' => '#FF6347',
    'TURQUOISE' => '#40E0D0',
    'VIOLET' => '#EE82EE',
    'WHEAT' => '#F5DEB3',
    'WHITE' => '#FFFFFF',
    'WHITE SMOKE' => '#F5F5F5',
    'YELLOW' => '#FFFF00',
    'YELLOW GREEN' => '#9ACD32'
);

/*
 * pages to be open with ssl
 */

$pages_to_be_open_with_ssl = array('checkout', 'account', 'login', 'register');

$navigationPages = array(
    //    'blog' => array('Blog', TRUE),
    'product' => array('Product', TRUE),
    'content' => array('Content Pages', TRUE),
    'shop' => array('Category', TRUE),
    'brand' => array('Brand', TRUE),
    'account' => array('Account', FALSE),
    'cart' => array('Cart', FALSE),
    'checkout' => array('Checkout', FALSE),
    'register' => array('Register', FALSE),
    'login' => array('Login', FALSE),
    'logout' => array('Logout', FALSE),
    'new_products' => array('Latest Products Page', FALSE),
    'on_sale' => array('Sale Page', FALSE),
    'top_products' => array('Best Seller Page', FALSE),
    'sitemap' => array('Sitemap Page', FALSE),
    'whishlist' => array('Wishlist Page', FALSE),
);
?>