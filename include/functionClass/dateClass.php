<?php
/*
 * Date Class - 
 * You are not adviced to make edits into this class.
 *   
 */

class date{
    
    public static function ToUKDate($date)
    {
            return date("d-m-Y", strtotime($date));
    }
   
    public static function ToUSDate($date)
    {

    $date= date("m/d/Y",strtotime($date));
    $Parts=array();
    $Parts=explode('/',$date);
    $Result=$Parts['2'].'-'.$Parts['0'].'-'.$Parts['1'];
    return $Result;
    }
    
    
    public static function add_days_to_date($days,$date,$mysql_format = false)
    {

            if($days==1):
                $date = strtotime(date("Y-m-d", strtotime($date)) . " +1 day");
            else:
                $date = strtotime(date("Y-m-d", strtotime($date)) . " +".$days." days");
            endif;
            
            if($mysql_format):
                $date_result=date('Y-m-d', $date );
            else:
                $date_result=date('d-m-Y', $date );
            endif;

            return $date_result;	
    }
    
    public static function add_weekdays_to_date($days,$date,$mysql_format = false)
    {

            if($days==1):
                $date = strtotime(date("Y-m-d", strtotime($date)) . " +1 weekday");
            else:
                $date = strtotime(date("Y-m-d", strtotime($date)) . " +".$days." weekdays");
            endif;
            
            if($mysql_format):
                $date_result=date('Y-m-d', $date );
            else:
                $date_result=date('d-m-Y', $date );
            endif;

            return $date_result;	
    }

    public static function subtract_days_from_date($days,$date)
    {

            if($days==1):
                $date = strtotime(date("Y-m-d", strtotime($date)) . " -1 day");
            else:
                $date = strtotime(date("Y-m-d", strtotime($date)) . " -".$days." days");
            endif;
            $date_result=date('y-m-d', $date );

            return $date_result;	
    }


    public static function dateDiff($start, $end) {

            $start_ts = strtotime($start);

            $end_ts = strtotime($end);

            if($end_ts>$start_ts):

                 $diff = $end_ts - $start_ts;

                 return round($diff / 86400);

            else:

                return 0;

            endif;


    }
    
    public static function ToFullDate($date)
    {
            return date("M d, Y \a\\t g.i a", strtotime($date));
    }
}

$date_obj= new date();

?>