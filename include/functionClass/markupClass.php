<?php
/*
 * city Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class markup extends cwebc {
    
    protected $orderby;
    protected $order;
    protected $requiredVars;
    
    /*
     * 
     */
    
    function __construct($order='desc', $orderby='id'){
        parent::__construct('markup');
            $this->orderby=$orderby;
            $this->order=$order;
            $this->requiredVars=array('id', 'user_id','markup');
    }

    /*
     * Create new city or update existing city
     */
    function saveMarkup($POST,$user_id){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
	$this->Data['user_id']=$user_id;
        //$this->Data['is_active']=isset($POST['is_active'])?"1":"0";
        if(isset($this->Data['id']) && $this->Data['id']!=''){
       
            if($this->Update())
              return $Data['id'];
        }
        else{
          
            $this->Insert();
            return $this->GetMaxId();
        }
    }
        
    /*
     * Get markup by id
     */
    function getMarkup($id){
        $this->Where=" where `id` = $id";
        return $this->DisplayOne();
    }
    
    /* get markup by user */	
    function getMarkupByUser($user_id){
        $this->Where=" where `user_id` = $user_id";
        return $this->DisplayOne();
    }	
    
    /* update markup */	
    function updateMarkup($id,$markup){
            $this->Data['id']=$id;
            $this->Data['markup']=$markup;
            if($this->Update()):
                return true;
            else:
                return false;
            endif;

        }
   /* delete markup */	
   function deleteMarkup($id){
        $this->id=$id;
        return $this->Delete();
    }		
    
    /*
     * Get List of all markup
     */
    function listMarkup($id){
            $this->Where=" where `id` = $id";
            return $query->ListOfAllRecords();        
    }
    
    
}
?>
