<?php

include_once(DIR_FS_SITE . 'include/functionClass/imageManipulationClass.php');

class content extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position', $parent_id = 0) {
        parent::__construct('content');
        $this->orderby = $orderby;
        $this->parent_id = $parent_id;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

//    function saveContent($POST) {
//        $this->Data = $this->_makeData($POST, $this->requiredVars);
//        if (isset($this->Data['id']) && $this->Data['id'] != '') {
//            if ($this->Update())
//                return $Data['id'];
//        }
//        else {
//            if ($this->Insert()) {
//                return $new_id = $this->GetMaxId();
//            } else {
//                return false;
//            }
//        }
//    }

    function saveContent($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $rand = rand(0, 99999999);
        $image_obj = new imageManipulation();
        if ($image_obj->upload_photo('content', $_FILES['image'], $rand)) {

            $this->Data['image'] = $image_obj->makeFileName($_FILES['image']['name'], $rand);
        }
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $id = $this->Data['id'];
            if ($this->Update()):
                return $id;
            endif;
        }
        else {
            if ($this->Insert()):
                return $this->GetMaxId();
            endif;
        }
        return false;
    }

    function deleteContent($id) {
        $this->id = $id;
        return $this->Delete();
    }

    function listContent($type) {
        if ($type != '') {
            $this->Where = "WHERE `type` = '$type'";
        }
        return $this->ListOfAllRecords();
    }

    function listContent_front($type) {
        $this->Where = "WHERE `type` = '$type'";
        return $this->ListOfAllRecords();
    }

    function list_content($id) {
        $this->Where = "WHERE `id` = '$id'";
        return $this->DisplayOne();
    }

    function setting($key) {
        $this->TableName = "cwebc_setting";
        $this->Where = "WHERE `key` = '$key'";
        $this->DisplayOne();
    }

}

?>