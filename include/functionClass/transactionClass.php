<?php

/*
 * User Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

//include_once(DIR_FS_SITE.'include/functionClass/serviceClass.php');
//include_once(DIR_FS_SITE.'include/functionClass/productClass.php');
//include_once(DIR_FS_SITE.'include/functionClass/moduleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/userMetaClass.php');

class transaction extends cwebc {

    function __construct($order = 'desc', $orderby = 'name') {
        parent::__construct('transaction');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'name', 'email', 'amount', 'currency', 'date', 'time', 'transaction', 'phone', 'is_recharge_done');
    }

    function saveTransactions($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function listAlltrasactions($user_id) {
        $this->Where = " where user_id='" . $user_id . "' ORDER BY `id` DESC";
        return $this->ListOfAllRecords('object');
    }

    function singletrasactions($id) {
        $this->Where = " where `id`='$id'";
        return $this->DisplayOne('object');
    }

    function update_field($field, $value, $id) {
        $this->Data['id'] = $id;
        $this->Data[$field] = $value;
        return $this->Update();
    }

}

?>