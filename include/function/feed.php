<?php

 function create_meta_title_feed($meta_title,$page_name='',$section='DEFAULT',$search_keyword=''){
        
        $section = strtoupper($section);
        
        $useful_meta = $meta_title;
        
        if($useful_meta==''):
            $useful_meta = $name;
        endif;
        
        $useful_meta = ucwords($useful_meta);
        
        global $PRODUCT_PAGE_META_TITLE;
        global $BRAND_PAGE_META_TITLE;
        global $CONTENT_PAGE_META_TITLE;
        global $CATEGORY_PAGE_META_TITLE;
        global $DEFAULT_PAGE_META_TITLE;
        global $SITE_NAME;
        global $META_TITLE_SEPARATOR;
        global $SEARCH_PAGE_META_TITLE;
        
        if($section=='PRODUCT'):
            $defined_pattern = $PRODUCT_PAGE_META_TITLE;
        elseif($section=='BRAND'):
            $defined_pattern = $BRAND_PAGE_META_TITLE;
        elseif($section=='CONTENT'):
            $defined_pattern = $CONTENT_PAGE_META_TITLE;
        elseif($section=='CATEGORY'):
            $defined_pattern = $CATEGORY_PAGE_META_TITLE;
        elseif($section=='SEARCH'):
            $defined_pattern = $SEARCH_PAGE_META_TITLE;
        else:
            $defined_pattern = $DEFAULT_PAGE_META_TITLE;
        endif;

        //$defined_values = array('%%SITENAME%%','%%SEP%%','%%META_TITLE%%','%%PAGE%%','%%SEARCH_KEYWORD%%');
        ///$new_values = array($SITE_NAME,$META_TITLE_SEPARATOR,ucwords($meta_title),ucwords($page_name),ucwords($search_keyword));
        //$new_meta_title = str_replace($defined_values, $new_values, $defined_pattern);
            
        $defined_values = array('%%SITENAME%%', '%%PRODUCT_NAME%%','%%PAGE_NAME%%','%%BRAND_NAME%%','%%CATEGORY_NAME%%','%%SEARCH_KEYWORD%%');
        $new_values = array(SITE_NAME,$useful_meta,$useful_meta,$useful_meta,$useful_meta,$useful_meta);
        $new_meta_title = str_replace($defined_values, $new_values, $defined_pattern);
            
        return $new_meta_title;
    }
    
?>
