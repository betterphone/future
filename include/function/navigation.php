<?php

/*
 * Navigation Functions
 *
 */

function get_navigation($id = 0, $coun = '') {
    $q = new query('navigation');
    if ($coun == 'USA'):
        $q->Where = "where is_active='1' and parent_id='$id' and (country='Both' or country='USA') order by position ";

    else:
        $q->Where = "where is_active='1' and parent_id='$id' and (country='Both' or country='Canada') order by position ";

    endif;
    $q->DisplayAll();
    if ($q->GetNumRows()) {
        return $q;
    }
    return false;
}

function get_navigation_options($query) {
    $q = new query('navigation');
    $q->Where = "where is_active='1' and parent_id='0' order by position ";
    $q->DisplayAll();
    echo '<option value="0">Root</option>';
    if ($q->GetNumRows()) {
        while ($object = $q->GetObjectFromRecord()) {
            if ($query != '' && $query == $object->id):
                echo '<option value="' . $object->id . '" selected>' . $object->navigation_title . '</option>';
            else:
                echo '<option value="' . $object->id . '">' . $object->navigation_title . '</option>';
            endif;
        }
    }
    return false;
}

function get_navname_by_id($id) {
    if ($id != 0):
        $q = new query('navigation');
        $q->Where = "where id='" . $id . "'";
        $r = $q->DisplayOne();
        return $r->navigation_title;
    else:
        return 'Root';
    endif;
}

function get_navigation_chain($page, $id = 0) {
    $QueryObj2 = new query('navigation');
    if ($id)
        $QueryObj2->Where = "where navigation_link='$page' and navigation_query='id=" . $id . "'";
    else
        $QueryObj2->Where = "where navigation_link='$page'";
    $object = $QueryObj2->DisplayOne();
    if (is_object($object)) {
        $name = get_object('navigation', $object->parent_id);
        if (is_object($name)) {
            return '<a href="' . make_url($name->navigation_link, $name->navigation_query) . '">' . $name->navigation_title . '</a>&nbsp;&rsaquo;&nbsp;' . $object->navigation_title;
        } else {
            return $object->navigation_title;
        }
    } else {
        $QueryObj3 = new query('content');
        $QueryObj3->Where = "where id='$id' ";
        $object2 = $QueryObj3->DisplayOne();
        return $object2->page_name;
    }
}

?>