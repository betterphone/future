<?php

include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

$user_id = $_SESSION['user_session']['user_id'];
$query = new user;
$user_info = $query->user_info($user_id);

if ($_SESSION['user_session']['user_id']) {
    if (isset($_POST['submit'])) {
//        $query = new user();
//        $old_password = $query->getOldPassword($_SESSION['user_session']['user_id'], md5($_POST['oldpassword']));
//        if ($old_password) {
        if ($_POST['newpassword'] != $_POST['confirmpassword']) {
            $admin_user->set_error();
            $admin_user->set_pass_msg('Password does not match.');
            Redirect(make_url('userpassword'));
        } else {
            $query = new user;
            $update = $query->changeUserPassword($_SESSION['user_session']['user_id'], md5($_POST['newpassword']));
            $admin_user->set_pass_msg('Password has been changed.');
            Redirect(make_url('home'));
        }
//        } else {
//            $admin_user->set_error();
//            $admin_user->set_pass_msg('Old password is wrong.');
//            Redirect(make_url('userpassword'));
//        }
    }

    if (isset($_POST['profile'])) {
        $_POST['id'] = $user_id;
        $query = new user;
        $query->saveUser($_POST);
        Redirect(make_url('userpassword'));
    }
} else
    Redirect(make_url('home'));
?>