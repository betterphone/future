<?php require 'header.inc.php'; ?>
<div id="blue">
    <div class="container">
        <div class="row centered">
            <div class="col-lg-8 col-lg-offset-2">
                <h4>WE CAN DO A LOT OF THINGS</b></h4>
                <p>JUST TAKE A LOOK & CONTACT US</p>
            </div>
        </div>
    </div>
</div>
<div class="container w">
    <div class="row centered">
        <br><br>
        <?php foreach ($services as $service) { ?>
            <div class="col-lg-4">
                <?php
                echo content_image($service['image']);
                ?>
                <h4><?php echo $service['name'] ?></h4>
                <p><?php echo $service['page'] ?></p>
            </div>
        <?php } ?>
    </div>
    <br>
</div>


<!-- PRICING SECTION -->
<!--<div id="dg">
    <div class="container">
        <div class="row centered">
            <h4>PRICING PLANS</h4>
            <br>
            <div class="col-lg-4">
                 START PRICING TABLE 
                <div class="pricing-option">
                    <div class="pricing-top">
                        <span class="pricing-edition">Starter</span>
                        <span class="price">
                            <sup>$</sup>
                            <span class="price-amount">19</span>
                            <small>/mo</small>
                        </span>
                    </div>
                    <ul>
                        <li><strong>10GB</strong> Storage</li>
                        <li>Up to <strong>10</strong> Users</li>
                        <li><strong>5</strong> Domains</li>
                        <li><strong>Free</strong> Setup</li>
                        <li><strong>1-year</strong> Free support *</li>
                    </ul>
                    <a href="index.html#" class="pricing-signup">Sign up</a>
                </div> /pricing-option 
                 END PRICING TABLE 
            </div> /col 

            <div class="col-lg-4">
                 START PRICING TABLE 
                <div class="pricing-option">
                    <div class="pricing-top">
                        <span class="pricing-edition">Standard</span>
                        <span class="price">
                            <sup>$</sup>
                            <span class="price-amount">29</span>
                            <small>/mo</small>
                        </span>
                    </div>
                    <ul>
                        <li><strong>20GB</strong> Storage</li>
                        <li>Up to <strong>20</strong> Users</li>
                        <li><strong>10</strong> Domains</li>
                        <li><strong>Free</strong> Setup</li>
                        <li><strong>2-year</strong> Free support *</li>
                    </ul>
                    <a href="index.html#" class="pricing-signup">Sign up</a>
                </div> /pricing-option 
                 END PRICING TABLE 
            </div> /col 

            <div class="col-lg-4">
                 START PRICING TABLE 
                <div class="pricing-option">
                    <div class="pricing-top">
                        <span class="special-label">OFFER!</span>
                        <span class="pricing-edition">Ultimate</span>
                        <span class="price">
                            <sup>$</sup>
                            <span class="price-amount">69</span>
                            <small>/mo</small>
                        </span>
                    </div>
                    <ul>
                        <li><strong>150GB</strong> Storage</li>
                        <li><strong>Unlimited</strong> Users</li>
                        <li><strong>50</strong> Domains</li>
                        <li><strong>Free</strong> Setup</li>
                        <li><strong>5-year</strong> Free support *</li>
                    </ul>
                    <a href="index.html#" class="pricing-signup">Sign up</a>
                </div> /pricing-option 
                 END PRICING TABLE 
            </div> /col 
        </div> row 
    </div> container 
</div> DG -->



<?php require 'footer.inc.php'; ?>