<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo DIR_WS_SITE . 'ico/favicon.png' ?>">

        <title><?php echo SITE_NAME ?></title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo DIR_WS_SITE . 'css/bootstrap.css' ?>" rel="stylesheet">
        <link href="<?php echo DIR_WS_SITE . 'css/font-awesome.min.css' ?>" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo DIR_WS_SITE . 'css/main.css' ?>" rel="stylesheet">

        <script src="<?php echo DIR_WS_SITE . 'js/Chart.js' ?>"></script>


        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="<?php echo DIR_WS_SITE . 'js/html5shiv.js' ?>"></script>
          <script src="<?php echo DIR_WS_SITE . 'js/respond.min.js' ?>"></script>
        <![endif]-->
    </head>

    <body>

        <!-- Fixed navbar -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo DIR_WS_SITE ?>"><?php echo SITE_NAME ?></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li <?php echo $page == 'home' ? 'class="active"' : '' ?>><a href="<?php echo make_url('home') ?>">HOME</a></li>
                        <li <?php echo $page == 'about' ? 'class="active"' : '' ?>><a href="<?php echo make_url('about') ?>">ABOUT</a></li>
                        <li <?php echo $page == 'service' ? 'class="active"' : '' ?>><a href="<?php echo make_url('service') ?>">SERVICES</a></li>
                        <li <?php echo $page == 'work' ? 'class="active"' : '' ?>><a href="<?php echo make_url('work') ?>">WORKS</a></li>
                        <li><a data-toggle="modal" data-target="#myModal" href="#myModal"><i class="fa fa-envelope-o"></i></a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>