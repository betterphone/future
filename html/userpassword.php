<?php require 'header.inc.php'; ?>
<div class="banner-bottom" style="min-height:500px;">
    <div class="container">
        <div class="view_plans" style="margin-top: 100px;">
            <h4 class="view_title">Change Password</h4><br>

            <div class="clearfix"> </div>
            <div class="col-md-4"></div>  
            <div class="col-md-4">
                <?php echo display_message(1); ?>
                <form action="<?php echo make_url('userpassword'); ?>" method="POST" class="form-signin">
                    <!--                                <label for="oldpassword" class="sr-only">Old Password</label>
                                                    <input type="password" name='oldpassword' id="oldpassword" class="form-control" placeholder="Old Password" required autofocus><br />-->
                    <label for="newpassword" class="sr-only">New Password</label>
                    <input type="password" name='newpassword' id="newpassword" class="form-control" placeholder="New Password" required autofocus><br />
                    <label for="confirmpassword"  class="sr-only">Confirm-Password</label>
                    <input type="password" name='confirmpassword' id="confirmpassword" class="form-control" placeholder="Confirm Password" required>

                    <br><br>
                    <button class="btn btn-lg btn-primary btn-block" name='submit' type="submit">Submit</button>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
    <div class="banner-bottom" style="min-height:500px;">
        <div class="container">
            <div class="view_plans">
                <h4 class="view_title">Profile</h4><br>

                <div class="clearfix"> </div>
                <div class="col-md-4"></div>  
                <div class="col-md-4">
                    <?php echo display_message(1); ?>
                    <form action="<?php echo make_url('userpassword'); ?>" method="POST" class="form-signin">
                        <label for="name" class="sr-only">Name</label>
                        <input type="text" name='name' id="name" class="form-control" placeholder="Name" required autofocus value="<?php echo $user_info ? $user_info->name : '' ?>" /><br />
                        <label for="confirmpassword"  class="sr-only">Email</label>
                        <input type="text" name='email' id="confirmpassword" class="form-control" placeholder="Email" required value="<?php echo $user_info ? $user_info->email : '' ?>" />

                        <br><br>
                        <button class="btn btn-lg btn-primary btn-block" name='profile' type="submit">Submit</button>
                    </form>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>


    <?php require 'footer.inc.php'; ?>  

