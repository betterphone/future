<?php require 'header.inc.php'; ?>
<div id="blue">
    <div class="container">
        <div class="row centered">
            <div class="col-lg-8 col-lg-offset-2">
                <h4>LEARN MORE ABOUT US</h4>
                <p>WE ARE COOL PEOPLE</p>
            </div>
        </div><!-- row -->
    </div><!-- container -->
</div><!--  bluewrap -->


<!--<div class="container w">
    <div class="row centered">
        <br><br>
        <div class="col-lg-3">
            <img class="img-circle" src="<?php echo DIR_WS_SITE . 'img/pic.jpg' ?>" width="110" height="110" alt="">
            <h4>Frank Lampard</h4>
            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
            <p><a href="#">@Frank_BlackTie</a></p>
        </div> col-lg-3 


    </div> row 
    <br>
    <br>
</div> container -->


<!-- PORTFOLIO SECTION -->
<div id="dg">
    <div class="container">
        <div class="row centered">
            <h4>OUR SKILLS</h4>
            <br>

            <?php foreach ($abouts as $about) { ?>
                <div class="col-lg-3">
                    <canvas id="canvas" height="130" width="130"></canvas>
                    <br>
    <!--                    <script>
                        var doughnutData = [
                            {
                                value: 70,
                                color: "#3498db"
                            },
                            {
                                value: 30,
                                color: "#ecf0f1"
                            }
                        ];
                        var myDoughnut = new Chart(document.getElementById("canvas").getContext("2d")).Doughnut(doughnutData);
                    </script>-->
                    <?php
                    echo content_image($about['image']);
                    ?>
                    <p><b><?php echo $about['name'] ?></b></p>
                    <p><small><?php echo $about['page'] ?></small></p>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php require 'footer.inc.php'; ?>