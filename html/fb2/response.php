<?php

include_once("config.php");
include_once("includes/functions.php");
include_once(dirname(dirname(dirname(__FILE__))) . '/include/config/config.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');
if (!$fbuser) {
    $fbuser = null;
    $loginUrl = $facebook->getLoginUrl(array('redirect_uri' => $homeurl, 'scope' => $fbPermissions));
    $output = '<a href="' . $loginUrl . '"><img src="images/fb_login.png"></a>';
} else {
    $user_profile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');
    $facebook_first_name = $user_profile['first_name'];
    $facebook_last_name = $user_profile['last_name'];
    $facebook_email = $user_profile['email'];
    $facebook_gender = $user_profile['gender'];
    $facebook_picture = $user_profile['picture']['data']['url'];
}

$post_array = array(
    'name' => $facebook_first_name . ' ' . $facebook_last_name,
    'email' => $facebook_email
);

$query = new user;
$checkUser = $query->checkUser($facebook_email);
if (!$checkUser) {
    $query = new user;
    $created_id = $query->saveUser($post_array);
    $_SESSION['user_session']['user_id'] = $created_id;
    $_SESSION['user_session']['email'] = $facebook_email;
    $_SESSION['user_session']['name'] = $facebook_first_name . ' ' . $facebook_last_name;
    $_SESSION['user_session']['logged_in'] = 1;
    $admin_user->set_pass_msg('You are now login and your account created with your fb information, please create a new password');
    Redirect(make_url('userpassword'));
} else {
    $_SESSION['user_session']['user_id'] = $checkUser->id;
    $_SESSION['user_session']['email'] = $facebook_email;
    $_SESSION['user_session']['name'] = $facebook_first_name . ' ' . $facebook_last_name;
    $_SESSION['user_session']['logged_in'] = 1;
    $admin_user->set_pass_msg('You are logged in now');
    Redirect(make_url('review'));
}
?>