<?php require 'header.inc.php'; ?>
<div id="blue">
    <div class="container">
        <div class="row centered">
            <div class="col-lg-8 col-lg-offset-2">
                <h4>WE WORK HARD TO ACHIEVE EXCELLENCE</h4>
                <p>AND WE ARE HAPPY TO DO IT</p>
            </div>
        </div>
    </div>
</div>
<div class="container desc">
    <?php foreach ($works as $work) { ?>
        <div class="row">
            <br><br>
            <div class="col-lg-6 centered">
                <?php
                echo content_image($work['image']);
                ?>
            </div><!-- col-lg-6 -->
            <div class="col-lg-6">
                <h4><?php echo $work['name'] ?></h4>
                <p>
                    <?php echo $work['page'] ?>
                </p>
    <!--            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                <p>
                    <i class="fa fa-circle-o"></i> Mobile Design<br/>
                    <i class="fa fa-circle-o"></i> Web Design<br/>
                    <i class="fa fa-circle-o"></i> Development<br/>
                    <i class="fa fa-link"></i> <a href="#">BlackTie.co</a>
                </p>-->
            </div>
        </div>
    <?php } ?>
    <br><br>
    <hr>
    <br><br>

    <br><br>
</div>
<?php require 'footer.inc.php'; ?>  