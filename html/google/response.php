<?php

include_once("config.php");
include_once("includes/functions.php");
include_once(dirname(dirname(dirname(__FILE__))) . '/include/config/config.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

if (isset($_REQUEST['code'])) {
    $gClient->authenticate();
    $_SESSION['token'] = $gClient->getAccessToken();
    header('Location: ' . filter_var($redirectUrl, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['token'])) {
    $gClient->setAccessToken($_SESSION['token']);
}

if ($gClient->getAccessToken()) {
    $userProfile = $google_oauthV2->userinfo->get();
    $_SESSION['token'] = $gClient->getAccessToken();
} else {
    $authUrl = $gClient->createAuthUrl();
}

$google_name = $userProfile['name'];
$google_email = $userProfile['email'];
$google_picture = $userProfile['picture'];
$google_gender = $userProfile['gender'];

$post_array = array(
    'name' => $google_name,
    'email' => $google_email
);

$query = new user;
$checkUser = $query->checkUser($google_email);
if (!$checkUser) {
    $query = new user;
    $created_id = $query->saveUser($post_array);
    $_SESSION['user_session']['user_id'] = $created_id;
    $_SESSION['user_session']['email'] = $google_email;
    $_SESSION['user_session']['name'] = $google_name;
    $_SESSION['user_session']['logged_in'] = 1;
    $admin_user->set_pass_msg('You are now login and your account created with your Google information, please create a new password');
    Redirect(make_url('userpassword'));
} else {
    $_SESSION['user_session']['user_id'] = $checkUser->id;
    $_SESSION['user_session']['email'] = $google_email;
    $_SESSION['user_session']['name'] = $google_name;
    $_SESSION['user_session']['logged_in'] = 1;
    $admin_user->set_pass_msg('You are logged in now');
    Redirect(make_url('home'));
}
?>