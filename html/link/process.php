<?php

include_once("config.php");
include_once("LinkedIn/http.php");
include_once("LinkedIn/oauth_client.php");
include_once(dirname(dirname(dirname(__FILE__))) . '/include/config/config.php');
include_once(DIR_FS_SITE . 'include/functionClass/userClass.php');

if (isset($_GET["oauth_problem"]) && $_GET["oauth_problem"] <> "") {
    $_SESSION["err_msg"] = $_GET["oauth_problem"];
    header("location:index.php");
    exit;
}

$client = new oauth_client_class;

$client->debug = false;
$client->debug_http = true;
$client->redirect_uri = $callbackURL;

$client->client_id = $linkedinApiKey;
$application_line = __LINE__;
$client->client_secret = $linkedinApiSecret;

if (strlen($client->client_id) == 0 || strlen($client->client_secret) == 0)
    die('Please go to LinkedIn Apps page https://www.linkedin.com/secure/developer?newapp= , ' .
                    'create an application, and in the line ' . $application_line .
                    ' set the client_id to Consumer key and client_secret with Consumer secret. ' .
                    'The Callback URL must be ' . $client->redirect_uri) . ' Make sure you enable the ' .
            'necessary permissions to execute the API calls your application needs.';

/* API permissions
 */
$client->scope = $linkedinScope;
if (($success = $client->Initialize())) {
    if (($success = $client->Process())) {
        if (strlen($client->authorization_error)) {
            $client->error = $client->authorization_error;
            $success = false;
        } elseif (strlen($client->access_token)) {
            $success = $client->CallAPI(
                    'http://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,location,picture-url,public-profile-url,formatted-name)', 'GET', array(
                'format' => 'json'
                    ), array('FailOnAccessError' => true), $user);
        }
    }
    $success = $client->Finalize($success);
}
if ($client->exit)
    exit;
if ($success) {
    $link_email = $user->emailAddress;
    $link_firstName = $user->firstName;
    $link_lastName = $user->lastName;
    $post_array = array(
        'name' => $link_firstName . ' ' . $link_lastName,
        'email' => $link_email
    );

    $query = new user;
    $checkUser = $query->checkUser($link_email);
    if (!$checkUser) {
        $query = new user;
        $created_id = $query->saveUser($post_array);
        $_SESSION['user_session']['user_id'] = $created_id;
        $_SESSION['user_session']['email'] = $link_email;
        $_SESSION['user_session']['name'] = $link_firstName . ' ' . $link_lastName;
        $_SESSION['user_session']['logged_in'] = 1;
        $admin_user->set_pass_msg('You are now login and your account created with your linkedin information, please create a new password');
        Redirect(make_url('userpassword'));
    } else {
        $_SESSION['user_session']['user_id'] = $checkUser->id;
        $_SESSION['user_session']['email'] = $link_email;
        $_SESSION['user_session']['name'] = $link_firstName . ' ' . $link_lastName;
        $_SESSION['user_session']['logged_in'] = 1;
        $admin_user->set_pass_msg('You are logged in now');
        Redirect(make_url('home'));
    }
} else {
    $_SESSION["err_msg"] = $client->error;
}
?>